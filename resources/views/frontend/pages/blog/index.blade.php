@extends('frontend.layouts.app')

@section('site_title', 'Blog')

@section('content')

    <div class="main-img">
        <img src="web/images/page-header-bg.jpg" alt="">
        <div class="overlay">
            <h2>Blog Grid 2 Columns</h2>
            <h4>Blog</h4>
        </div>
    </div>
    <section>
        <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mt-2">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Blog</li>
                    </ol>
                    <hr>
                </nav>
                <div class='content blog-area'>


                    

                    <ul class="nav nav-pills mb-3  justify-content-center my-sizer-element" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <label>
                                <input type="radio" class="d-none" name="category" value="all" checked>
                                <span class="nav-link">All Blog Posts</span>
                            </label>
                        </li>
                        @foreach($categories as $slug => $name)
                        <li class="nav-item" role="presentation">
                            <label>
                                <input type="radio" name="category" value="{{ $slug }}" class="d-none">
                                <span class="nav-link">{{ $name }}</span>
                            </label>
                        </li>
                        @endforeach
                        <!-- <li class="nav-item" role="presentation">
                            <button class="nav-link " id="pills-shopping-tab" data-bs-toggle="pill" data-bs-target="#pills-shopping" type="button" role="tab" aria-controls="pills-shopping" aria-selected="false">Shopping</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link " id="pills-fashion-tab" data-bs-toggle="pill" data-bs-target="#pills-fashion" type="button" role="tab" aria-controls="pills-fashion" aria-selected="false">Fashion</button>
                        </li> -->
                        
                    </ul>
                    <div>
                        <div>
                            <div class="row my-shuffle-container">
                                @foreach($blogs as $b)
                                <div class="entry-item col-sm-6" data-groups='["{{ @$b->web_category->slug }}"]'>
                                    <div class="card mb-3 blog-card">
                                        <a href="{{route('blog.show',1)}}">
                                            <img src="{{url('web/images/post-1.jpg')}}" class="card-img-top" alt="..." style="width: 100%; height:100%;">
                                        </a>
                                        @php
                                        // date('F d, Y', strtotime($post->created_at));
                                        @endphp
                                        <div class="t-body py-4">
                                            <div class="h-body text-center">
                                                <span class="mx-2">|</span>
                                                <a href="{{route('blog.show',1)}}">January 12, 2022 </a>
                                                <span class="mx-2">|</span>
                                            </div>
                                            <h2 class="t-title text-center">
                                                <a href="{{route('blog.show',1)}}">{{$b -> title}}</a>
                                            </h2>
                                            <div class="content-body text-center">
                                                <p>{{ $b-> excerpt}}</p>
                                                <a href="{{route('blog.show', $b->slug)}}" class="cont-reading">Continue Reading</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <div class="col-1 my-sizer-element"></div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>

    </section>

@endsection

@section('extra_scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/Shuffle/6.0.0/shuffle.min.js" integrity="sha512-fN+YeX/572qBnWu5Axj90DDyKjSNUfvR+8OaFkz27xnr+QE50vZHefJ+bplRrM284a8cJgMLxEipmKshf8vrug==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script>
    const Shuffle = window.Shuffle;
    const element = document.querySelector('.my-shuffle-container');
    const sizer = element.querySelector('.my-sizer-element');

    const shuffleInstance = new Shuffle(element, {
        itemSelector: '.entry-item',
        sizer: sizer // could also be a selector: '.my-sizer-element'
    });

    $('input[name="category"]').on('change', function (evt) {
        var input = evt.currentTarget;
        if (input.checked) {
            shuffleInstance.filter(input.value);
        }
    });

</script>
@endsection