@extends('frontend.layouts.app')

@section('site_title', 'Blog')

@section('content')

<div class="main-img">
            <img src="{{url('web/images/page-header-bg.jpg')}}" alt="">
            <div class="overlay">
                <h2>Blog Grid 2 Columns</h2>
                <h4>Blog</h4>
            </div>
    </div>
    <section>
        <div class="container">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb mt-2">
                        <li class="breadcrumb-item"><a href="{{url('/')}}">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{route('blog.index')}}">Blog</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Blog Post</li>
                    </ol>
                    <hr>
                </nav>
                
                <div class="content">
                
                    <div class="blog-image text-center pt-5 pb-5">
                        <img src="{{url('web/images/post-1.jpg')}}" alt="">

                        <div class="content-text">
                            <div class="h-datetime pt-2 text-left">
                                <span>January 12, 2022</strong>
                            </div>
                        </div>
                    </div>
                    <div class="main-content mb-5">
                            <p>{{ $blog-> description}}</p>
                        </div>
                     
                </div>

        </div>

    </section>

@endsection