@extends('frontend.layouts.app')

@section('site_title', 'IAgancy')

@section('content')

    <section class="slider-part">
        <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
            <div class="carousel-indicators">
                @foreach($sliders as $i => $s)
                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="{{ $i }}" class="{{ $i == 0 ? 'active' : '' }}" aria-current="true" aria-label="{{ $s->title }}"></button>
                @endforeach
            </div>
            <div class="carousel-inner">

                @foreach($sliders as $i => $s)

                <div class="carousel-item{{$i == 0 ? ' active' : ''}}">
                    <img src="{{url('storage/'.$s -> image)}}" class="d-block w-100" style="height: 600px; width: 100%; object-fit: cover;" alt="...">
                    <div class="carousel-caption d-none d-md-block">
                        <!-- <h5>{{$s ->title}}</h5>
                        <p>{{$s ->excerpt}}</p> -->
                    </div>
                </div>
                @endforeach
            </div>
            <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
            </button>
            <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
            </button>
        </div>

    </section>

    <!-- About Section Starts -->
    <section style="background-color: #31383e" id="section1">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="ab-section text-white">
                        <span>About Us</span>
                        <div class="py-5">
                            <img src="{{url('web/images/ins.png')}}" alt="" style="height: 140px;">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="">
                        <h2 class="ab-heading text-end">
                            <span>
                                InsAgy puts you in Control
                            </span>
                        </h2>
                    </div>
                    <p class="text-white text-end py-2">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsa nisi dolore libero voluptates nobis beatae tempora eveniet? Provident quod, placeat eum, quo blanditiis similique, accusantium deleniti unde in iure aliquam?
                    </p>
                </div>
            </div>
            <div>

            </div>
            
        </div>
    </section>
    <!-- About Section Ends -->

    <!-- Product Starts -->
    <section class="pb-5" id="section2">
        <div class="head-img">
            <img src="{{url('web/images/download.svg')}}" alt="">
        </div>
        <div class="container">
            <ul class="nav nav-pills mb-3 py-1 justify-content-center" id="pills-tab" role="tablist">
                @foreach($products as $i => $p)
                <li class="nav-item" role="presentation">
                    <button class="nav-link{{ $i == 0 ? ' active' : '' }}" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-{{ $p->id }}" type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{ $p->title }}</button>
                </li>
                @endforeach
               
                
            </ul>
            <div class="tab-content py-5" id="pills-tabContent">
                @foreach($products as $i => $p)
                <div class="tab-pane fade {{ $i == 0 ? 'show active' : ''}}" id="pills-{{ $p->id }}" role="tabpanel" aria-labelledby="pills-home-tab">
                    <div class="row">
                        <div class="col-lg-6 text-center">
                            <img src="{{url('storage/' . $p->image)}}" alt="" style="max-width: 100%;">
                        </div>
                        <div class="col-lg-6">
                            <div class="p-heading1">
                                
                                
                                
                                 {!! $p -> excerpt !!}

                                <div class="product-btn">
                                    <a href="{!! $p -> url !!}" target="_blank">
                                        <span>Donwload App</span>
                                    </a>
                                </div>
                                
                                
                            </div>
                        </div>
                    </div>
                    
                </div>

                @endforeach

            </div>

            <div class="text-center m-auto w-50">
                <p class="p-style">
                    <span>InsAgy works for all types of insurance agencies and businesses. Whether you sell life, health, motor vehicle, fire, burglary it doesn’t matter. We give you the tools to manage and automate your business for greater success! </span>
                </p> 
           </div>
        </div>
    </section>
    <!-- Product ends -->
    
    <!-- Price -->
    <section id="section3">
        <div class="container pt-5">
            <div class="text-center">
                <h2 class="price-heading">
                    <span>Pricing</span>
                </h2>
            </div>
            <div class="price-para">
                <div class="text-center m-auto w-50">
                <p>
                    <span>Simple &</span>
                    <strong>Flexible</strong>
                    <span>enough to match your professional growth</span>
                </p>
                </div>
            </div>
            <div>
                <ul class="nav nav-pills mb-3 py-1 justify-content-center" id="pills-tab" role="tablist">
                    @foreach($products as $i => $p)
                    <li class="nav-item" role="presentation">
                        <button class="nav-link{{ $i == 0 ? ' active' : '' }}" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills2-{{ $p->id }}" type="button" role="tab" aria-controls="pills-home" aria-selected="true">{{ $p->title }}</button>
                    </li>
                    @endforeach
                    
                    
                </ul>
                <div class="tab-content py-5" id="pills-tabContent">
                    @foreach($products as $i=> $p)
                    <div class="tab-pane fade {{ $i == 0 ? 'show active' : ''}}" id="pills2-{{ $p->id }}" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row">
                            <div class="col-lg-6 text-center">
                                <img src="{{url('web/images/scr1.png')}}" alt="" style="max-width: 100%;">
                            </div>
                            <div class="col-lg-6">
                                    @if($p->web_pricing)
                                    <div class="price-details text-center">
                                        <h4>{{$p -> web_pricing->title}}</h4>
                                        <h1>{{$p -> web_pricing->price}}</h1>
                                            {!!$p -> web_pricing->description !!}
                                    </div>
                                    @endif
                            </div>
                        </div>
                        
                    </div>
                    @endforeach
                    
                </div>
            </div>
        </div>
    </section>
    <!-- Price Starts-->

    <!-- Request Demo -->
    <section class="pb-5 req-demo" id="section4">
        <div class="head-img1">
            <img src="{{url('web/images/download1.svg')}}" alt="">
        </div>
        <div class="bg-clr">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                    {{ Form::open(['url' => route('request.store'), 'method'=>'POST', 'files' => true, 'class' => 'user']) }}
                        <div class="req-heading">
                            <h2>
                                <span>Request A Demo</span>
                            </h2>
                        </div>
                        <div class="mb-3">

                            {{Form::select('web_product_id',$all_products, '', ['class' => 'squareInput form-control py-3', 'placeholder'=>'Select Product','required'=>'required'])}}
                        </div>

                        <div class="mb-3">
                            <!-- <label for="exampleFormControlInput1" class="form-label">Name</label> -->
                            <!-- <input type="text" class="form-control py-3" id="exampleFormControlInput1" placeholder="Enter Name"> -->
                            {{Form::text('name', '', ['class' => 'form-control py-3', 'placeholder'=>'Enter name','required'=>'required'])}}
                            
                        </div>
                        <div class="mb-3">
                            <!-- <label for="exampleFormControlInput1" class="form-label">Email</label> -->
                            <!-- <input type="email" class="form-control py-3" id="exampleFormControlInput1" placeholder="name@email.com"> -->
                            {{Form::text('email', '', ['class' => 'form-control py-3', 'placeholder'=>'name@email.com','required'=>'required'])}}
                        </div>
                        <div class="mb-3">
                            <!-- <label for="exampleFormControlInput1" class="form-label">Mobile</label> -->
                            <!-- <input type="number" class="form-control py-3" id="exampleFormControlInput1" placeholder="Contact"> -->
                            {{Form::text('mobile', '', ['class' => 'form-control py-3', 'placeholder'=>'Contact','required'=>'required'])}}
                        </div>
                        <div class="mb-3">
                            <!-- <label for="exampleFormControlTextarea1" class="form-label">Message</label> -->
                            <!-- <textarea class="form-control py-3" id="exampleFormControlTextarea1" rows="3" placeholder="Enter Message"></textarea> -->
                            {!! Form::textarea('description', null, ['class' => 'form-control py-3','placeholder' => 'Enter Message', 'rows' => '2'])!!}
                        </div>

                        <button type="submit" class="btn btn-lg contact-btn my-3">Submit</button>
                        {{ Form::close() }}
                    </div>
                    <div class="col-lg-6">
                        <div class="rd-img">
                            <img src="web/images/main_image.svg" alt="">
                        </div>
                    </div>
                </div>    
            </div>
        </div>  
        <div class="foot-img">
            <img src="{{url('web/images/download2.svg')}}" alt="">
        </div>
    </section>
     
    <!-- Request Demo Ends-->

    <!-- Testimonial -->
    <section>

        <div class="container pt-5 pb-5">
            <div class="test-title">
                <h2>
                    <span>Check out what clients say</span>
                </h2>
                <i class="bi bi-quote"></i>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-3 testi-content">
                    <p class="text-center m-auto">
                        <span>“I was shocked at how much time we were spending on daily tasks instead of selling insurance!  We started using InsAgy and by cutting all of those tasks out of our daily routine, we were able to spend our time meeting new Prospects!  Within a matter of months we had doubled our client base because we had the time to focus on growing our agency!”</span>
                    </p>
                    <hr>
                    <h4>We Insure You</h4>
                </div>
                <div class="col-lg-3 testi-content1">
                    <p class="text-center m-auto">
                        <span>“I was trying to keep track of everything on sticky notes and notebooks. Our team was always dropping the ball and to be completely honest, my agency was an unorganised mess. Since I started using InsAgy, I’m organised, efficient and we’ve never missed a renewal reminder for any of my clients!”</span>
                    </p>
                    <hr>
                    <h4>Secure Insurance Services</h4>

                </div>
            </div>
        </div>
    </section>
    <!-- Testimonial Ends-->
    <!-- Contact -->
    <section class="pt-5 pb-5" style="background-color: #e8e8e8" id="section5">
        <div class="container">
            <div class="row">
                <div class="col-lg-6">
                    <div class="contact-heading">
                        <h2>
                            <span>Contact Us</span>
                        </h2>
                    </div>
                    <form action="">
                        <div class="mb-3">
                            <!-- <label for="exampleFormControlInput1" class="form-label">Email</label> -->
                            <input type="text" class="form-control pt-3 pb-3" id="exampleFormControlInput1" placeholder="Name">
                            
                        </div>
                        <div class="mb-3">
                            <!-- <label for="exampleFormControlInput1" class="form-label">Mobile</label> -->
                            <input type="email" class="form-control pt-3 pb-3" id="exampleFormControlInput1" placeholder="Email">
                        </div>
                        <div class="mb-3">
                            <!-- <label for="exampleFormControlInput1" class="form-label">Mobile</label> -->
                            <input type="number" class="form-control pt-3 pb-3" id="exampleFormControlInput1" placeholder="Contact No.">
                        </div>
                        <div class="mb-3">
                            <!-- <label for="exampleFormControlTextarea1" class="form-label">Message</label> -->
                            <textarea class="form-control pt-3 pb-3" id="exampleFormControlTextarea1" rows="3" placeholder="Message"></textarea>
                        </div>
                    </form>
                    <button type="button" class="btn btn-lg contact-btn"><i class="bi bi-envelope-fill px-1"></i>Send Message</button>
                </div>
                <div class="col-lg-6">
                    <div class="contact-details">
                        <h4 class="pb-2">
                            <span>+91 {{$site -> mobile}}</span>
                        </h4>
                        <h4 class="pb-2">
                            <span>{{$site -> address}}</span>
                        </h4>
                        <a href="$site -> instagram_url"><i class="bi bi-instagram"></i></a>
                        <a href="$site -> facebook_url"><i class="bi bi-facebook"></i></a>
                        <a href="$site -> twitter_url"><i class="bi bi-twitter"></i></a>
                       
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Contact end -->
@endsection