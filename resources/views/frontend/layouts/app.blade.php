<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="stylesheet" href="{{url('web/css/style.css')}}">
    <link rel="stylesheet" href="{{url('web/css/responsive.css')}}">
    <link rel="shortcut icon" href="{{url('images/setting/favicon/'.$site->favicon)}}" type="image/x-icon">
    <title>@yield('site_title', $site->title)</title>
</head>

<body>

    <header class="header-part">
            <nav class="navbar navbar-expand-lg">
                <div class="container">
                    <a class="navbar-brand" href="#">
                        <div class="logo">
                            <img src="{{url('images/setting/logo/'.$site->logo)}}" alt="">
                        </div>
                    </a>
                    <button class="navbar-toggler head-btn" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="bi bi-list"></i>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarNav">
                        <ul class="navbar-nav ms-auto menu-bar">
                            <li class="nav-item">
                                <a class="nav-link active" aria-current="page" href="#section1">About Us</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link"  href="#section2">Products</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#section3">Pricing</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('blog.index')}}" target="_blank">Blog</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#section4">Request A Demo</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#section5">Contact Us</a>
                            </li>
                        </ul>
                        <div class="dropdown">
                            <button class="btn" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Sign In
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                                
                            </ul>
                        </div>
                        <div class="dropdown">
                            <button class="btn" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                                Sign Up
                            </button>
                            <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                                <li><a class="dropdown-item" href="#">Action</a></li>
                                <li><a class="dropdown-item" href="#">Another action</a></li>
                           
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
      
    </header>

    @yield('content')

    <footer style="background-color: #e8e8e8">
        <div class="container">

            <div class="text-center pb-2 pt-2 footer-style">
                <a href="" class="px-2">Terms</a>
                <a href="" class="px-2">Privacy Policy</a>
                <a href="" class="px-2">Push Notificataion</a>
                <a href="" class="px-2">FAQ's</a>
                
            </div>
            <!-- <div class="row">
            <div class="col-lg-6 justify-content-end">
                <i class="bi bi-telephone-fill"></i>+91 9910045737
            </div>
            <div class="col-lg-6">
                <i class="bi bi-telegram"></i>
            </div>
            </div> -->
            
        </div>
        <a href="#" class="scroll-aaraow"><i class="bi bi-arrow-up-circle"></i></a>
    </footer>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>    
    @yield('extra_scripts')
</body>
</html>