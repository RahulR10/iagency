<style>
    * {
        box-sizing: border-box;
    }
    .head {
        font-weight: 400px;
    }

    table {
        width: 100%;
        font-size: 12px;
    }

    table tr th {
        border: 1px solid;
        padding: 5px;
        margin: 0;
        text-align: left;
    }

    table {
        border-collapse: collapse;
    }

    table td {
        border: 1px solid black;
        padding: 5px;
    }

    table tr:first-child td {
        border-top: 0;
    }

    table tr td:first-child {
        border-left: 0;
    }

    table tr:last-child td {
        border-bottom: 0;
    }

    table tr td:last-child {
        border-right: 0;
    }

    .footer table tr td,
    .footer table tr th {
        /*background-color: #8e44ad;*/
        color: #8e44ad;
        border: 0px solid;
        padding: 10px;
        margin: 0;
        font-weight: bold !important;
    }
    
    @page {
        margin: 0;
    }
</style>
<div>
    <div class="head" style="margin-bottom: 30px; background: #f9f9f9; padding: 30px;">
        <div style="float: left; width: 75%">
            <h3 style="padding: 15px 0; margin: 0;">{{ @$quotation->company->name }}</h3>
        </div>
        <div style="float: right; width: 20%; text-align: right;">
            <img src="{{ url('images/company/'. @$quotation->company->image) }}" alt="" style="max-height: 50px; max-width: 100%">
        </div>
    </div>
    
    <div style="padding: 30px;">
        <div style="text-align: right;  border: 2px 2px 2px 0px; padding: 2px; font-size: 18px; font-weight: bold;">

        <span>Final Premium : </span>
        <span>{{ round($quotation->data->part_c_final_premium->final_premium) }}</span>
    </div>
    <div style="clear:both;"></div>
    <table style="margin-bottom: 10px;">
        <tr>
            <td>Customer Name</td>
            <th>{{ @$quotation->data->customer_detail->customer_name }}</th>
            <td>Mobile</td>
            <td width="160">{{ @$quotation->data->customer_detail->customer_mobile }}</td>
        </tr>
    </table>
    <table>
        <tr>
            <td>Vehicle Make </td>
            <td></td>
            <td>Model</td>
            <td></td>
        </tr>
        <tr>
            <td>Registration No.</td>
            <td>{{ @$quotation->data->customer_detail->customer_vehicle }}</td>
            <th style="font-size: 18px">IDV</th>
            <th style="font-size: 18px">{{ @$quotation->data->part_a_own_damage->idv }}</th>
        </tr>
        <tr>
            <td>Policy Type</td>
            <td>{{ @$quotation->data->basic_details->policy_type }}</td>
            <td>Type of Vehivle</td>
            <td>{{ @$quotation->data->basic_details->type_of_vehivle }}</td>
        </tr>
        <tr>
            @if($quotation->type_id == 1 || @$quotation->type_id == 2 || @$quotation->type_id == 7 || @$quotation->type_id == 8 || @$quotation->type_id == 9 || @$quotation->type_id == 10 || @$quotation->type_id == 11 || @$quotation->type_id == 12 || @$quotation->type_id == 13)
            <td>Vehicle's Cubic Capacity</td>
            <td>{{ @$quotation->data->basic_details->cubic_capacity }}</td>
            @endif
            @if($quotation->type_id == 3 || @$quotation->type_id == 14 || @$quotation->type_id == 15 || @$quotation->type_id == 16 || @$quotation->type_id == 17 || @$quotation->type_id == 4 || @$quotation->type_id == 5)
            <td>GVW</td>
            <td>{{ @$quotation->data->basic_details->gvw }}</td>
            @endif
            @if($quotation->type_id == 6 || @$quotation->type_id == 21)
            <td>Vehicle Category</td>
            <td>{{ @$quotation->data->basic_details->vehicle_cat }}</td>
            @endif
            <td>RTO</td>
            <td>{{ @$quotation->data->basic_details->trto }}</td>
        </tr>
        <tr>
            <td>Age of Vehicle</td>
            <td>{{ @$quotation->data->basic_details->age_of_vehicle }}</td>
            <td>Zone</td>
            <td>{{ @$quotation->data->basic_details->zone }}</td>
        </tr>
        @if($quotation->type_id == 7 || @$quotation->type_id == 8 || @$quotation->type_id == 9 || @$quotation->type_id == 10 || @$quotation->type_id == 11 || @$quotation->type_id == 12 || @$quotation->type_id == 13)
        <tr>
            <td>Seating Capacity</td>
            <td>{{ @$quotation->data->basic_details->seating_cap }}</td>
            <td>Carrying Capacity</td>
            <td>{{ @$quotation->data->basic_details->carrying_cap }}</td>
        </tr>
        @endif
        <tr>
            @if($quotation->type_id == 2 && ($quotation->type_id == 3 || @$quotation->type_id == 3 || @$quotation->type_id == 14 || @$quotation->type_id == 15 || @$quotation->type_id == 16 || @$quotation->type_id == 17 || @$quotation->type_id == 4 || @$quotation->type_id == 5) && @$quotation->type_id == 7 || @$quotation->type_id == 8 || @$quotation->type_id == 9 || @$quotation->type_id == 10 || @$quotation->type_id == 11 || @$quotation->type_id == 12 || @$quotation->type_id == 13)
            <td>CNG / LPG</td>
            <td>{{ @$quotation->data->basic_details->cng_lpg }}</td>
            @endif
            @if($quotation->type_id == 7 || @$quotation->type_id == 8 || @$quotation->type_id == 9 || @$quotation->type_id == 10 || @$quotation->type_id == 11 || @$quotation->type_id == 12 || @$quotation->type_id == 13)
            <td>Vehicle Category</td>
            <td>{{ @$quotation->data->basic_details->vehicle_cat }}</td>
            @endif
        </tr>
    </table>
    @if($quotation->type_id == 1)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Liability Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ @$quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ @$quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ @$quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->discount }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ @$quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->loading }}</td>
            <td>PA to Unnamed Passanger</td>
            <td>{{ @$quotation->data->part_b_liability->pa_to_unnamed_passanger }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
            <td>TPPD Cover</td>
            <td>{{ @$quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ @$quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <th>Net Liability Premium (B)</th>
            <th>{{ @$quotation->data->part_b_liability->final_tp }}</th>
        </tr>
        <tr>
            <td>Non Electrical/Electronics Accessories</td>
            <td>{{ @$quotation->data->part_a_own_damage->non_electric_accessories_value }}</td>
        </tr>
        <tr>
            <td>Basic OD Before NCB</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_od_before_ncb }}</td>
        </tr>
        <tr>
            <th>No Claim Bonus</th>
            <th>{{ @$quotation->data->part_a_own_damage->no_claim_bonus }}</th>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <th>{{ @$quotation->data->part_a_own_damage->final_own_damage }}</th>
        </tr>
    </table>
    @endif
    @if($quotation->type_id == 2)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Liability Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ @$quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ @$quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>Bi-Fuel System</td>
            <td>{{ @$quotation->data->part_b_liability->bi_fuel_system }}</td>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->discount }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ @$quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->loading }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ @$quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
            <td>PA to Unnamed Passanger</td>
            <td>{{ @$quotation->data->part_b_liability->pa_to_unnamed_passanger }}</td>
        </tr>
        <tr>
            <td>CNG / LPG Value</td>
            <td>{{ @$quotation->data->part_a_own_damage->cng_lpg_value }}</td>
            <td>TPPD Cover</td>
            <td>{{ @$quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ @$quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <th>Net Liability Premium (B)</th>
            <th>{{ @$quotation->data->part_b_liability->final_tp }}</th>
        </tr>
        <tr>
            <td>Non Electrical/Electronics Accessories</td>
            <td>{{ @$quotation->data->part_a_own_damage->non_electric_accessories_value }}</td>
        </tr>
        <tr>
            <td>Basic OD Before NCB</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_od_before_ncb }}</td>
        </tr>
        <tr>
            <th>No Claim Bonus</th>
            <th>{{ @$quotation->data->part_a_own_damage->no_claim_bonus }}</th>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>RSA</td>
            <td>{{ @$quotation->data->part_a_own_damage->rsa }}</td>
        </tr>
        <tr>
            <td>Return to Invoice</td>
            <td>{{ @$quotation->data->part_a_own_damage->return_invoice }}</td>
        </tr>
        <tr>
            <td>Egnine Protector</td>
            <td>{{ @$quotation->data->part_a_own_damage->engine_pro }}</td>
        </tr>
        <tr>
            <td>Cousumables</td>
            <td>{{ @$quotation->data->part_a_own_damage->cousumables }}</td>
        </tr>
        <tr>
            <td>NCB Protector</td>
            <td>{{ @$quotation->data->part_a_own_damage->ncb_pro }}</td>
        </tr>
        <tr>
            <td>Tyre Cover</td>
            <td>{{ @$quotation->data->part_a_own_damage->tyre_cover }}</td>
        </tr>
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <th>{{ @$quotation->data->part_a_own_damage->final_own_damage }}</th>
        </tr>
    </table>
    @endif
    @if($quotation->type_id == 3 || @$quotation->type_id == 14 || @$quotation->type_id == 15 || @$quotation->type_id == 16 || @$quotation->type_id == 17 || @$quotation->type_id == 4 || @$quotation->type_id == 5)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Liability Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ @$quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ @$quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>CNG Amount (TP)</td>
            <td>{{ @$quotation->data->part_b_liability->cng_amt }}</td>
        </tr>
        <tr>
            <td>CNG / LPG Value</td>
            <td>{{ !empty($quotation->data->part_a_own_damage->cng_value) ? @$quotation->data->part_a_own_damage->cng_value : 0 }}</td>
            <td>Geo Ext. TP</td>
            <td>{{ @$quotation->data->part_b_liability->geo_tp }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ @$quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ @$quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>Geo Ext.</td>
            <td>{{ @$quotation->data->part_a_own_damage->geo_ext }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ @$quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>IMT-23</td>
            <td>{{ @$quotation->data->part_a_own_damage->imt }}</td>
            <td>LL to Emp.</td>
            <td>{{ @$quotation->data->part_b_liability->ll_employee }}</td>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->discount }}</td>
            <td>PA to Paid Driver</td>
            <td>{{ @$quotation->data->part_b_liability->pa_paid }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->loading }}</td>
            <td>TPPD Cover</td>
            <td>{{ @$quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
            <th>Net Liability Premium (B)</th>
            <th>{{ @$quotation->data->part_b_liability->final_tp }}</th>
        </tr>
        <tr>
            <td>Basic OD Before NCB</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_od_before_ncb }}</td>
        </tr>
        <tr>
            <th>No Claim Bonus</th>
            <th>{{ @$quotation->data->part_a_own_damage->no_claim_bonus }}</th>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>RSA</td>
            <td>{{ @$quotation->data->part_a_own_damage->rsa }}</td>
        </tr>
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <th>{{ @$quotation->data->part_a_own_damage->final_own_damage }}</th>
        </tr>
    </table>
    @endif
    @if($quotation->type_id == 7 || @$quotation->type_id == 8 || @$quotation->type_id == 9 || @$quotation->type_id == 10 || @$quotation->type_id == 11 || @$quotation->type_id == 12 || @$quotation->type_id == 13)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Liability Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ @$quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ @$quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>Passenger TP</td>
            <td>{{ @$quotation->data->part_b_liability->passenger_tp }}</td>
        </tr>
        <tr>
            <td>CNG / LPG Value</td>
            <td>{{ @$quotation->data->part_a_own_damage->cng_value }}</td>
            <td>bi_fuel_system</td>
            <td>{{ @$quotation->data->part_b_liability->bi_fuel_system }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ @$quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <td>Geo Ext. TP</td>
            <td>{{ @$quotation->data->part_b_liability->geo_tp }}</td>
        </tr>
        <tr>
            <td>Geo Ext.</td>
            <td>{{ @$quotation->data->part_a_own_damage->geo_ext }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ @$quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>IMT-23</td>
            <td>{{ @$quotation->data->part_a_own_damage->imt }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ @$quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->discount }}</td>
            <td>LL to Employee</td>
            <td>{{ @$quotation->data->part_b_liability->ll_employee }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->loading }}</td>
            <td>PA to Paid Driver</td>
            <td>{{ @$quotation->data->part_b_liability->pa_paid }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
            <td>TPPD Cover</td>
            <td>{{ @$quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>Basic OD Before NCB</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_od_before_ncb }}</td>
            <th>Net Liability Premium (B)</th>
            <th>{{ @$quotation->data->part_b_liability->final_tp }}</th>
        </tr>
        <tr>
            <th>No Claim Bonus</th>
            <th>{{ @$quotation->data->part_a_own_damage->no_claim_bonus }}</th>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>RSA</td>
            <td>{{ @$quotation->data->part_a_own_damage->rsa }}</td>
        </tr>
        @if($quotation->type_id == 7 || @$quotation->type_id == 12 || @$quotation->type_id == 13)
        <tr>
            <td>Egnine Protector</td>
            <td>{{ @$quotation->data->part_a_own_damage->engine_pro }}</td>
        </tr>
        
        <tr>
            <td>Cousumables</td>
            <td>{{ @$quotation->data->part_a_own_damage->cousumables }}</td>
        </tr>
        @endif
        
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <th>{{ @$quotation->data->part_a_own_damage->final_own_damage }}</th>
        </tr>
    </table>
    @endif
    @if($quotation->type_id == 6 || @$quotation->type_id == 21)
    <table style="margin-top: 15px;">
        <tr>
            <th colspan="2">Own Damage Premium(A)</th>
            <th colspan="2">Liability Premium(B)</th>
        </tr>
        <tr>
            <td>Basic for vehicle</td>
            <td>{{ @$quotation->data->part_a_own_damage->vehicle_basic_rate }}</td>
            <td>Basic TP</td>
            <td>{{ @$quotation->data->part_b_liability->basic_tp }}</td>
        </tr>
        <tr>
            <td>Basic Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_own_damage }}</td>
            <td>CPA Owner Driver</td>
            <td>{{ @$quotation->data->part_b_liability->cpa_owner_driver }}</td>
        </tr>
        <tr>
            <td>Electrical/Electronics Accessories</td>
            <td>{{ @$quotation->data->part_a_own_damage->electric_accessories_value }}</td>
            <td>LL to Paid Driver</td>
            <td>{{ @$quotation->data->part_b_liability->ll_to_paid_driver }}</td>
        </tr>
        <tr>
            <td>IMT-47</td>
            <td>{{ @$quotation->data->part_a_own_damage->imt_fs }}</td>
            <td>LL to Employee</td>
            <td>{{ @$quotation->data->part_b_liability->ll_employee }}</td>
        </tr>
        <tr>
            <td>IMT-23</td>
            <td>{{ @$quotation->data->part_a_own_damage->imt }}</td>
            <td>TPPD Cover</td>
            <td>{{ @$quotation->data->part_b_liability->tppd_cover }}</td>
        </tr>
        <tr>
            <td>Discount Before</td>
            <td>{{ @$quotation->data->part_a_own_damage->discount_before }}</td>
            <th>Net Liability Premium (B)</th>
            <th>{{ @$quotation->data->part_b_liability->final_tp }}</th>
        </tr>
        <tr>
            <td>Discount on OD premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->discount }}</td>
        </tr>
        <tr>
            <td>Loading on OD Premium</td>
            <td>{{ @$quotation->data->part_a_own_damage->loading }}</td>
        </tr>
        <tr>
            <td>Basic OD after Discount/Loading</td>
            <td>{{ @$quotation->data->part_a_own_damage->basic_od_after_discount }}</td>
        </tr>
        <tr>
            <th>No Claim Bonus</th>
            <th>{{ @$quotation->data->part_a_own_damage->no_claim_bonus }}</th>
        </tr>
        <tr>
            <td>Gross Own Damage</td>
            <td>{{ @$quotation->data->part_a_own_damage->gross_own_damage }}</td>
        </tr>
        <tr>
            <td>Zero Dep (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->zero_dep }}</td>
        </tr>
        <tr>
            <td>Other Add-On (%)</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on_percent }}</td>
        </tr>
        <tr>
            <td>Other Add-On</td>
            <td>{{ @$quotation->data->part_a_own_damage->other_add_on }}</td>
        </tr>
        <tr style="margin-top: 5px;">
            <th>Net Own Damage Premium (A) </th>
            <th>{{ @$quotation->data->part_a_own_damage->final_own_damage }}</th>
        </tr>
    </table>
    @endif
    <table style="margin-top: 15px;">
        <tr></tr>
        <tr>
            <th>Premium before GST (A+B)</th>
            <th>{{ @$quotation->data->part_c_final_premium->premium_before_taxes }}</th>
        </tr>
        @if($quotation->type_id == 1 || @$quotation->type_id == 2 || @$quotation->type_id == 6 || @$quotation->type_id == 21)
        <tr>
            <th>GST @18% </th>
            <th>{{ @$quotation->data->part_c_final_premium->gst }}</th>
        </tr>
        @endif
        @if($quotation->type_id == 3 || @$quotation->type_id == 14 || @$quotation->type_id == 15 || @$quotation->type_id == 16 || @$quotation->type_id == 17 || @$quotation->type_id == 4 || @$quotation->type_id == 5)
        <tr>
            <th>GST @12% </th>
            <th>{{ @$quotation->data->part_c_final_premium->gst_twelve }}</th>
        </tr>
        <tr>
            <th>GST @18% </th>
            <th>{{ @$quotation->data->part_c_final_premium->gst_eighteen }}</th>
        </tr>
        @endif
        @if($quotation->type_id == 7 || @$quotation->type_id == 8 || @$quotation->type_id == 9 || @$quotation->type_id == 10 || @$quotation->type_id == 11 || @$quotation->type_id == 12 || @$quotation->type_id == 13)
        <tr>
            <th>GST @18% </th>
            <th>{{ @$quotation->data->part_c_final_premium->gst_eighteen }}</th>
        </tr>
        @endif
        <tr>
            <th>Other Cess</th>
            <th>{{ @$quotation->data->part_c_final_premium->kerala_cess }}</th>
        </tr>
        <tr>
            <th>Total </th>
            <th>{{ $total = round($quotation->data->part_c_final_premium->final_premium, 2) }}</th>
        </tr>
        <tr>
            <td>Round off </th>
            <td>{{ number_format(round($total) - $total, 2) }}</td>
        </tr>
        <tr>
            <th>Final Premium </th>
            <th>{{ round($total) }}</th>
        </tr>
    </table>

    <div style="padding-top: 5px;">
        <div style="padding-top: 7px;">
            <p style="margin: 0px; padding: 0px; font-size: 13px;">Kindly pay cheque/DD in favor of</p>
            <h4 style="margin-top: 0;">{{ @$quotation->company->name }}</h4>
        </div>
        <div>
            <p style="margin: 0px; padding: 0px; font-size: 13px;">Documents Required:-</p>
            <table>
                <tr>
                    <td>1. Previous Policy Copy</td>
                    <td>2. RC copy</td>
                    <td>3. Other as requirement</td>
                </tr>
            </table>
            <p style="font-size: 13px; font-weight: bold;">Note : In case of any claim, NCB will be revised and hence Quotation is Subject to Change.</p>
            <p style="font-size: 13px; font-weight: bold;">Quote Validity: This Quote is valid till midnight</p>
            <p style="text-align:center; font-size: 12px;">Insurance is subject matter of the solicitation.</p>
        </div>
        <div class="footer">
            <table>
                <tr>
                    <td>Adviser Name</td>
                    <td>:</td>
                    <td>{{ $user->sign ? $user->sign->name : '' }}</td>
                    <td>Adviser Contact</td>
                    <td>:</td>
                    <td>{{ $user->sign ? $user->sign->mobile : '' }}</td>
                    <td rowspan="2" style="width: 25%;">
                        <div style="min-height: 200px; object-fit: contain; border: 1px solid #ddd;">
                            @if($user->image)
                            <img src="{{ url('/').'/images/profile/'.$user->image }}" style="width: 100%;" />
                            @endif
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>Adviser Email ID</td>
                    <td>:</td>
                    <td>{{ $user->sign ? $user->sign->email : '' }}</td>
                    <td>Quote Date</td>
                    <td>:</td>
                    <td>{{ @$quotation->date }}</td>
                </tr>
            </table>
        </div>
        
        <div style="background: #2980b9; color: #fff; margin-top: 15px; padding: 10px 15px; border-radius: 100px; text-align: center; font-weight: bold">
            This quotation is generated from iAgancy Motor Insurance Premium Calculator.
        </div>
    </div>

    
</div>