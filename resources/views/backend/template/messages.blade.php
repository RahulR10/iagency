@if($errors->any())
<div class="alert alert-danger">
    {!! implode(' ',$errors->all('<div>:message</div>')) !!}
</div>
@endif

@if(Session::has('success'))
<p class="alert alert-success">{{ \Session::get('success') }}</p>
@endif

@if(Session::has('danger'))
<p class="alert alert-danger">{{ \Session::get('danger') }}</p>
@endif