@extends('backend.layout.app')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Pricing</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item active">Pricing List</li>
                    </ul>
                </div>
            </div>
        </div>
        @include('backend.template.messages')
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2 class="pt-2"><b>Pricing List</b></h2>
                        <h2 class="header-dropdown m-r--5"><a href="{{ route('webadmin.price.create') }}" class="btn btn-primary" style="padding-top: 8px;">Add Page</a></h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example contact_list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>Product</th>
                                        <th>Price</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                    $sn = 1;
                                    @endphp
                                    @foreach($prices as $key => $price)
                                    <tr>
                                        <td>{{ $key + $prices->firstItem() }}</td>
                                        <td>{{ $price->title }}</td>
                                        <td>{{ @$price->web_products -> title}}</td>
                                        <td>{{ $price->price}}</td>
                                        <td>{{ $price->description}}</td>
                                        <td>
                                            <button class="btn tblActnBtn">
                                                <a href="{{ route('webadmin.price.edit',$price->id) }}" style="color: black;"><i class="material-icons">mode_edit</i></a>
                                            </button>
                                            {{ Form::open(['url' => route('webadmin.price.destroy',$price), 'method' => 'DELETE','class' => 'btn tblActnBtn delete-form']) }}
                                            <button type="button" class="btn tblActnBtn">
                                                <a  style="color: black;"><i class="material-icons delete-btn">delete</i></a>
                                            </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            {{ $prices->appends(request()->except('page'))->links('pagination::bootstrap-4')  }}
        </div>
    </div>
</section>

@endsection