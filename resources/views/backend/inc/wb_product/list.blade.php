@extends('backend.layout.app')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Product</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item active">Product List</li>
                    </ul>
                </div>
            </div>
        </div>
        @include('backend.template.messages')
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2 class="pt-2"><b>Product List</b></h2>
                        <h2 class="header-dropdown m-r--5"><a href="{{ route('webadmin.product.create') }}" class="btn btn-primary" style="padding-top: 8px;">Add Product</a></h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example contact_list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>URL</th>
                                        <th>Image</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {{-- @dd($product) --}}
                                    @foreach($products as $key => $product)
                                    <tr>
                                        <td>{{ $key + $products->firstItem() }}</td>
                                        <td>{{ $product->title }}</td>
                                        <td>{{ $product->url }}</td>
                                        <td>
                                        <img src="{{ url('storage/' . $product->image) }}" alt="" style="width: 100px; height: 100px; object-fit: contain;">
                                        </td> 
                                        <td>
                                            <button class="btn tblActnBtn">
                                                <a href="{{ route('webadmin.product.edit',$product->id) }}" style="color: black;"><i class="material-icons">mode_edit</i></a>
                                            </button>
                                            {{ Form::open(array('url' => route('webadmin.product.destroy',$product), 'class' => 'btn tblActnBtn delete-form')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            <button type="button" class="btn tblActnBtn delete-btn">
                                                <a style="color: black;"><i class="material-icons">delete</i></a>
                                            </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection