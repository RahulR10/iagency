@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">

  <div class="col-lg-6">
    {{Form::label('type', 'Select Type'), ['class' => 'active']}}
    {{Form::select(
      'type_id', $type ,'0', 
      ['class' => 'form-control squareInput vtype',
      'style'=> 'border:1px solid;' ,
      'data-target'=>'#make_id',
      'data-apiurl' => route('api.vtype_web.index'),
      'placeholder'=>'Select Type',
      'required'=>'required'])}}
  </div>

  <div class="col-lg-6">
    {{Form::label('make_id', 'Select Make'), ['class' => 'active']}}
    {{Form::select('make_id', $make ,null, ['class' => 'form-control squareInput','style'=> 'border:1px solid;','placeholder'=>'Select Make','required'=>'required'])}}
  </div>

  <div class="col-lg-6">
    {{Form::text('name', '', ['class' => 'squareInput aslug', 'placeholder'=>'Enter Vehicle Model','required'=>'required'])}}
    {{Form::label('name', 'Enter Vehicle Model'), ['class' => 'active']}}
  </div>

</div>