<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Quoatation List</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item active">Quoatation List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">

                    <div class="body">
                        
                        {{ Form::open(['method'=>'get']) }}
                        <div class="row">
                            
                            <div class="col-sm-2 mb-3">
                                {{ Form::select('user_id',$users,null,['class'=>'form-control','placeholder'=>'Select User']) }}
                            </div>

                            <div class="col-sm-2 mb-3">
                                {{ Form::select('type_id',$types,null,['class'=>'form-control','placeholder'=>'Select Type']) }}

                            </div>

                            <div class="col-sm-2 mb-3">
                                <select name="limit" class="form-control mr-4">
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                    <option value="500">500</option>
                                </select>
                            </div>

                            <div class="col-sm-4 mb-3">
                                <button type="submit" class="btn btn-primary px-5">Filter</button>
                                <a href="{{  route('admin.quotation.index') }}" class="btn btn-warning px-5">Clear</a>
                            </div>
                        </div>
                        {{ Form::close() }}

                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example contact_list">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>User Name</th>
                                        <th>Mobile</th>
                                        <th>Type</th>
                                        <th>Creation Date</th>
                                        <th>Updated date</th>
                                        <th>Pdf</th>
                                        {{-- <th>Action</th> --}}
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($quotations as $key => $quotation)
                                    <tr>
                                        <td>{{ $key + $quotations->firstItem() }}</td>
                                        <td>{{ @$quotation->user->name }}</td>
                                        <td>{{ @$quotation->user->mobile }}</td>
                                        <td>{{ @$quotation->type->name }}</td>
                                        <td>{{ date("d-m-Y h:m:A", strtotime($quotation->created_at)) }}</td>
                                        <td>{{ date("d-m-Y h:m:A ", strtotime($quotation->updated_at)) }}</td>
                                        <td><a href="{{ url('/').'/pdf/'. $quotation->pdf }}" target="_blank">View Pdf</a></td>
                                        <td>
                                            {{-- <button class="btn tblActnBtn">
                                                <a href="{{ route('admin.slider.edit',$list->id) }}" style="color: black;"><i class="material-icons">mode_edit</i></a>
                                            </button>
                                            {{ Form::open(array('url' => route('admin.slider.destroy',$list->id), 'class' => 'btn tblActnBtn')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            <button class="btn tblActnBtn">
                                                <a style="color: black;"><i class="material-icons">delete</i></a>
                                            </button>
                                            {{ Form::close() }} --}}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div>
                            <div class="paginate" id="paginate">
                            {{ $quotations->appends(Request::all())->links('pagination::bootstrap-4')  }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>