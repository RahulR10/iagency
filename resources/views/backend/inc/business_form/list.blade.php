<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Business Form Detail Page</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item active">Business Form List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="body">                        
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example contact_list" id="dataTable">
                                <thead>   
                                    <tr>
                                       
                                        <th>S.N.</th>
                                        <th>User Name</th>
                                        <th>User Mobile</th>
                                        <th>Vehicle Category</th>
                                        <th>Vehicle Sub Category</th>
                                        <th>Customer Name</th>
                                        <th>Vehicle Number</th>
                                        <th>Insurer Name</th>
                                        <th>Policy Type</th>
                                        <th>Make</th>
                                        <th>Modal</th>
                                        <th>Fuel</th>
                                        <th>Payout In on</th>
                                        <th>Policy Start Date</th>
                                        <th>Policy End Date</th>
                                        <th>Last Updated Date</th>
                                        <th>Created Date</th>
                                        <th>Policy Pdf</th>
                                        <th>More</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>

<script>
    $(function() {
        var table = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.business_form.index') }}",
            columns: [
                {data: 'DT_RowIndex', name:'index'},
                {data: 'user_name', name:'user_name'},
                {data: 'user_mobile', name:'user_mobile'},
                {data: 'vehicle_category_name', name: 'vehicle_category_name'},
                {data: 'vehicle_subcategory_name', name: 'vehicle_subcategory_name'},
                {data: 'customer_name', name: 'customer_name'},
                {data: 'vehicle_number', name: 'vehicle_number'},
                {data: 'insurer_name', name: 'insurer_name'},
                {data: 'vehicle_policy_type', name: 'vehicle_policy_type'},
                {data: 'make_name', name: 'make_name'},
                {data: 'model_name', name: 'model_name'},
                {data: 'fuel_type', name: 'fuel_type'},
                {data: 'payoutIn', name: 'payoutIn'},
                {data: 'policy_start_date', name: 'policy_start_date'},
                {data: 'policy_end_date', name: 'policy_end_date'},
                {data: 'last_update', name: 'last_update'},
                {data: 'created_date', name: 'created_date'},
                {data: 'policy_pdf', name: 'policy_pdf'},
                {data: 'show', name:'show'},
            ],
            "lengthMenu" : [[10, 20, 50, 100, 200, 500, -1], [10, 20, 50, 100, 200, 500, "All"]]
        })
        let sort = _.orderBy(table,['desc']);
    })
</script>