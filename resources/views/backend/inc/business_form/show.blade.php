<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Quoatation View</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item active">Business Form View</li>
                    </ul>
                </div>
            </div>
        </div>

       <div class="table-responsive">
            <table class="table-bordered">
                <tr>
                    <td colspan="6">
                        <div style="text-align: end; fon-size: 20px;">
                            <strong style=" font-size: 20px ;">User Name : 
                                {{ $business_form-> user->name }}
                            </strong> 
                        </div>
                    </td>
                </tr>
                <tr>
                    <th>Customer Name</th>
                    <td>{{ @$business_form->title }} {{ @$business_form->full_name }}</td>
                    <th>Mobile No.:</th>
                    <td>{{ @$business_form->mobile }} </td>
                    <th>Email :</th>
                    <td>{{ $business_form -> email }}</td>
                </tr>
                <tr>
                    <th>Address</th>
                    <td style="width: 200px;" > {{ $business_form -> address }} {{ $business_form -> pincode }} </td>
                    <th>City :</th>
                    <td> {{ $business_form -> city }} </td>
                    <th>State :</th>
                    <td>{{ $business_form -> state }} </td>
                </tr>
            </table>

            <table class="table-bordered mt-5">
                <tr>
                    <th>Vehicle Type</th>
                    <td>{{ @$business_form->vehicle_type }}</td>
                    <th>Vehicle Catagory :</th>
                    <td><span>{{ @$business_form-> vehicle_category_name }}</span>
                        @if($business_form->vehicle_subcategory_name)
                        <br>&laquo; <small>{{  $business_form->vehicle_subcategory_name }}</small>
                        @endif
                    </td>
                    <th>Vehicle Make :</th>
                    <td> {{ @$business_form->make_name }} </td>
                </tr>
                <tr>
                    <th>Vehicle Model</th>
                    <td> {{ @$business_form->model_name }} </td>
                    <th>Fuel:</th>
                    <td>{{ @$business_form->fuel_type }} </td>
                    <th>Varient:</th>
                    <td>{{ @$business_form->variant_name }}</td>
                </tr>
                <tr>
                    <th>Cubic Capacity</th>
                    <td> {{ @$business_form->cubic_capacity }}</td>
                    <th>Seating Capacity:</th>
                    <td>{{ @$business_form->seating_capacity }}</td>
                    <th>Vehicle No. :</th>
                    <td>{{ @$business_form->vehicle_number }}</td>
                </tr>

                <tr>
                    <th>Registration Date</th>
                    <td>{{ @$business_form->reg_date }}</td>
                    <th>Career</th>
                    <td>{{ $business_form -> carrier }}</td>
                    <th>RTO </th>
                    <td>{{ $business_form -> rto_name }}</td>
                </tr>

                <tr>
                    <th>Zone</th>
                    <td>{{ @$business_form->zone }}</td>
                    <th>Engine Number</th>
                    <td>{{ @$business_form->engine_number }}</td>
                    <th>Chassis Number</th>
                    <td>{{ @$business_form->chassis_number }}</td>
                </tr>
            </table>

            <table class="table-bordered mt-5">
                <tr>
                    <th>Insurer Name</th>
                    <td>{{ @$business_form->insurer_name }}</td>
                    <th>Previous Insurer Name</th>
                    <td>{{ @$business_form->prev_insurer_name }}</td>
                    <th>IDV :</th>
                    <td>{{ @$business_form->idv }}</td>
                    
                </tr>
                <tr>
                    <th>Policy Type</th>
                    <td>{{ @$business_form->vehicle_policy_type }}</td>
                    <th>Previous Policy Available :</th>
                    <td>{{ @$business_form-> is_prev_policy }}</td>
                    <th>Policy Start Date</th>
                    <td> {{ @$business_form->policy_start_date }} </td>
                </tr>
                <tr>
                    <th>Policy end date:</th>
                    <td>{{ @$business_form->policy_end_date }} </td>
                    <th>Previous Policy Claim:</th>
                    <td>{{ @$business_form->prev_policy_claim }} </td>
                    <th>Policy Number :</th>
                    <td> {{ @$business_form->policy_number }} </td>
                </tr>
                <tr>
                    <th>Previous Policy Number :</th>
                    <td> {{ @$business_form->prev_policy_number }} </td>
                    <th>Previous NCB :</th>
                    <td> {{ @$business_form->prev_ncb }} </td>
                    <th>Current NCB :</th>
                    <td> {{ @$business_form->current_ncb }} </td>
                </tr>
                <tr>
                    <th>Addons Policy</th>
                    <td>{{ $business_form->addons }}</td>
                    <th>Is Financed</th>
                    <td>{{ $business_form->is_financed }}</td>
                    <th>Financed Company</th>
                    <td>{{ $business_form->finance_company_name }}</td>
                </tr>

                <tr>
                    <th>OD PREMIUM:</th>
                    <td> {{ @$business_form->od_premium }} </td>
                    <th>TP PREMIUM </th>
                    <td> {{ @$business_form->tp_premium }} </td>
                    <th>NET Premium :</th>
                    <td>{{ @$business_form->net_premium }} </td>
                </tr>

                <tr>
                    <th>GST :</th>
                    <td> {{ @$business_form->gst_cess }} </td>
                    <th>Final Premium:</th>
                    <td>{{ @$business_form-> final_premium }}</td>
                    <th>Customer Type :</th>
                    <td> {{ @$business_form->customer_type }} </td>
                </tr>

            </table>

            <table class="table-bordered mt-5">
                <tr>
                    <th>Broker Name </th>
                    <td>{{ @$business_form->broker_name }}</td>
                    <th>Sub Agent Name :</th>
                    <td>{{ @$business_form-> sub_agent_name }}</td>
                    <th>Commission Receivable :</th>
                    <td> {{ @$business_form->commission_receivable }} </td>
                </tr>
                <tr>
                    <th>Commission Receivable Amount</th>
                    <td> {{ @$business_form->commission_receivable_amount }} </td>
                    <th>Cp Od Premimum</th>
                    <td> {{ @$business_form->cp_od_premium }} </td>
                    <th>Cp Tp Premimum</th>
                    <td> {{ @$business_form->cp_tp_premium }} </td>
                </tr>
               
                <tr>
                    <th>Cp Net Premimum</th>
                    <td> {{ @$business_form->cp_net_premium }} </td>
                    <th>Cp Total</th>
                    <td> {{ @$business_form->cp_net_premium }} </td>
                    <th>Policy Pdf</th>
                    <td> <a href="{{  $business_form->policy_pdf_url }}" target="_blank">View Pdf</a> </td>
                </tr>
            </table>

            <table class="table-bordered mt-5 mb-5">
                <tr>
                    <th>Payout In:</th>
                    <td>{{ @$business_form-> payout_in }}</td>
                    <th>Payout In Status :</th>
                    <td>{{ @$business_form-> payout_in_status }}</td>
                    <th>PayOut :</th>
                    <td> {{ @$business_form->payout_out }} </td>
                </tr>

                <tr>
                    <th>PayOut Status :</th>
                    <td>{{ $business_form -> payout_out_status }}</td>
                    <th>Profit & Loss :</th>
                    <td>{{ $business_form -> pnl }}</td>
                </tr>
            </table>
       </div>
       
    </div>
</section>