<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">User List</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item active">User List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2 class="float-left"><b>User List</b></h2>
                            {{-- <form method="get" class="float-right" style="display: flex;">
                                <select name="limit" class="form-control mr-4">
                                    <option value="20">20</option>
                                    <option value="50">50</option>
                                    <option value="100">100</option>
                                    <option value="200">200</option>
                                    <option value="500">500</option>
                                </select>
                               
                                <input name="s" type="text" class="form-control mr-4" placeholder="Search......">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form> --}}
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover" id="dataTable">
                                <thead>
                                    <tr>
                                        <th>Sr No.</th>
                                        <th>Name</th>
                                        <th>User Name</th>
                                        <th>Email</th>
                                        <th>Mobile</th>
                                        <th>Address</th>
                                        <th>Referal Code</th>
                                        <th>Referal User</th>
                                        <th>Registration Date</th>
                                        {{-- <th>Creation Date</th> --}}
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                </tbody>
                            </table>

                        </div>
                            
                                {{-- <div class="paginate" id="paginate">
                                {{$lists->appends(Request::all())->links("pagination::bootstrap-4")}}
                                </div> --}}
                        </div>

                        <!-- The modal -->
                        <div class="modal fade" id="flipFlop" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        {{ Form::open(['method' => 'PUT', 'files' => true, 'id' => 'userEditForm']) }}
                                            <label for="">Validation Date</label>
                                            <input type="date" name="valid_upto" id="valid_upto" value="" min="<?php echo date("Y-m-d"); ?>" >

                                           
                                            
                                            {{ Form::select('subscription_plan_id', $subscription_plan, '0',['class' => 'form-control', 'placeholder' => 'Select Plan', 'id' => 'subscription_plan_id']) }}
                                            
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-secondary">Update</button>
                                                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                            </div>
                                        {{ Form::close() }}
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                        {{-- End Modal --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script>
    $(function () {
        var table = $('#dataTable').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{ route('admin.users.index') }}",
            columns: [
                {data: 'DT_RowIndex', name: 'index'},
                {data: 'name', name: 'name'},
                {data: 'user_name', name: 'user_name'},
                {data: 'email', name: 'email'},
                {data: 'mobile', name: 'mobile'},
                {data: 'full_address', name: 'full_address'},
                {data: 'referal_code', name: 'referal_code'},
                {data: 'full_referal', name: 'full_referal'},
                {data: 'date', name: 'date' },
                // {data: 'update_date', name: 'update_date'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ],
            "lengthMenu" : [[10, 20, 50, 100, 200, 500, -1], [10, 20, 50, 100, 200, 500, "All"]] 
        });

        $(document).on('click', '.toggle-user-edit-modal', function () {
            $('#flipFlop').modal('show');

            $('#userEditForm').attr('action', $(this).data('url'));
            $('#valid_upto').val($(this).data('valid'));
            $('#subscription_plan_id').val($(this).data('plan')).trigger('change');
        });
    });
</script>