<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Vehicle Info</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin.vw_vehicle.index') }}">Vehicles</a>
                        </li>
                        <li class="breadcrumb-item active">Vehicle Info</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2 class="pt-2"><b>Vehicle Info</b></h2>
                        <!-- <h2 class="header-dropdown m-r--5"><a href="{{ route('admin.vw_feedback.create') }}" class="btn btn-primary" style="padding-top: 8px;">Add User</a></h2> -->
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example contact_list">
                                <tr>
                                    <th>Name</th>
                                    <td>{{ $list->user ? $list->user->first_name : ''}} {{ $list->user ? $list->user->last_name : ''}}</td>
                                    <th>Mobile</th>
                                    <td>{{ $list->user ? $list->user->mobile : ''}}</td>
                                </tr>
                                <tr>
                                    <th>Reg. Number</th>
                                    <td style="text-transform:uppercase">{{ $list->reg_number }}</td>
                                    <th>Category</th>
                                    <td>{{ $list->category ? $list->category->name : ''}}</td>
                                </tr>
                                
                            </table>
                            <div class="header ml-0 pl-0">
                                <h2 class="pt-2"><b>Vehicle Documents</b></h2>
                                <!-- <h2 class="header-dropdown m-r--5"><a href="{{ route('admin.vw_feedback.create') }}" class="btn btn-primary" style="padding-top: 8px;">Add User</a></h2> -->
                            </div>
                            <table class="table table-hover js-basic-example contact_list">
                                <tr>
                                    <th>Document</th>
                                    <th>Expiry Date</th>
                                    <th>image</th>
                                </tr>
                                @foreach($list->vehicle_documents as $vd)
                                <tr>
                                    
                                    <td style="text-transform:uppercase">{{$vd->document ? $vd->document->name : ''}}</td>
                                    <td>{{ $vd->expiry_date ?? 'NA' }}</td>
                                    <td class="table-img" style="width: 100px;">
                                        <a href="{{ url('/').'/storage/'. $vd->image }}" target="_blank">
                                            <img src="{{ url('/').'/storage/'. $vd->image }}" alt="">
                                        </a>
                                    </td>
                                </tr>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>