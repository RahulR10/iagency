<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Make List</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item active">Make List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    @include('backend.template.messages')
                    <div class="header">
                        <h2 class="pt-2"><b>Make List</b></h2>
                        <h2 class="header-dropdown m-r--5"><a href="{{ route('admin.make.create') }}" class="btn btn-primary" style="padding-top: 8px;">Add Make</a></h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example contact_list">
                                <thead>
                                    <tr>
                                        <th>Sr No. </th>
                                        <th>Type</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($makes as $key => $make)
                                    <tr>
                                        <td>{{ $key + $makes->firstItem() }}.</td>
                                        <td>{{ @$make-> type ->name }}</td>
                                        <td>{{ $make->name }}</td>
                                        <td>
                                            <button class="btn tblActnBtn">
                                                <a href="{{ route('admin.make.edit',$make->id) }}" style="color: black;"><i class="material-icons">mode_edit</i></a>
                                            </button>

                                            {{ Form::open(array('url' => route('admin.make.destroy',$make->id), 'class' => 'btn tblActnBtn delete-form')) }}

                                            {{ Form::hidden('_method', 'DELETE') }}

                                            <button type="button" class="btn delete-btn tblActnBtn">
                                                <a style="color: black;"><i class="material-icons">delete</i></a>
                                            </button>

                                            {{ Form::close() }}

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div>
                            {{ $makes->appends(request()->except('page'))->links('pagination::bootstrap-4')  }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>