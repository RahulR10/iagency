@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif

<div class="row">

  <div class="col-lg-6">
    {{Form::label('type_id', 'Select Type'), ['class' => 'active']}}
    {{Form::select('type_id', $type ,null, 
    ['class' => 'form-control squareInput vtype',
    'style'=> 'border:1px solid;' ,
    'data-target'=>'#make_id',
    'data-apiurl' => route('api.vtype_web.index'),
    'placeholder'=>'Select Type',
    'required'=>'required'])}}
  </div>

  <div class="col-lg-6">
    {{Form::label('make', 'Select Make'), ['class' => 'active']}}
    {{Form::select('make_id', $make ,null, 
    ['class' => 'form-control squareInput make',
    'data-target'=>'#vmodel_id',
    'id' => 'make_id',
    'data-apiurl' => route('api.make_web.index'),
    'style'=> 'border:1px solid;' ,
    'placeholder'=>'Select Make',
    'required'=>'required'])}}
  </div>

  <div class="col-lg-6">
    {{Form::label('vwmodel_id', 'Select Model'), ['class' => 'active']}}
    {{Form::select('vwmodel_id', $vwmodel ,null, [
      'class' => 'form-control squareInput vmodel',
      'id' => 'vmodel_id',
      'style'=> 'border:1px solid;' ,
      'placeholder'=>'Select Model',
      'required'=>'required'
      ])}}
  </div>

  <div class="col-lg-6">
    {{Form::label('type', 'Select Fuel'), ['class' => 'active']}}
    {{Form::select('vwmfuel_type_id', $vw_fuel_type ,'0', ['class' => 'form-control squareInput','style'=> 'border:1px solid;' ,'placeholder'=>'Select Fuel','required'=>'required'])}}
  </div>

  <div class="col-lg-6">
    {{Form::text('name', '', ['class' => 'squareInput', 'placeholder'=>'Enter Vehicle Variant','required'=>'required'])}}
    {{Form::label('name', 'Enter Vehicle Variant'), ['class' => 'active']}}
  </div>

  <div class="col-lg-6">
    {{Form::text('cubic_capacity', '', ['class' => 'squareInput', 'placeholder'=>'Enter Cubic Capacity','required'=>'required'])}}
    {{Form::label('cubic_capacity', 'Enter Cubic Capacity'), ['class' => 'active']}}
  </div>
 
  <div class="col-lg-6">
    {{Form::text('seating_capacity', '', ['class' => 'squareInput', 'placeholder'=>'Enter Seating Capacity','required'=>'required'])}}
    {{Form::label('seating_capacity', 'Enter Seating Capacity'), ['class' => 'active']}}
  </div>

</div>