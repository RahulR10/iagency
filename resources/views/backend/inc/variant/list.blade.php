<section class="content">
    <div class="container-fluid">
        <div class="block-header">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <ul class="breadcrumb breadcrumb-style ">
                        <li class="breadcrumb-item">
                            <h4 class="page-title">Variant List</h4>
                        </li>
                        <li class="breadcrumb-item bcrumb-1">
                            <a href="{{ route('admin-home') }}">
                                <i class="fas fa-home"></i> Home</a>
                        </li>
                        <li class="breadcrumb-item active">Variant List</li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="header">
                        <h2 class="pt-2"><b>Variant List</b></h2>
                        <h2 class="header-dropdown m-r--5"><a href="{{ route('admin.variant.create') }}" class="btn btn-primary" style="padding-top: 8px;">Add Variant</a></h2>
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-hover js-basic-example contact_list">
                                <thead>
                                    <tr>
                                        <th>Sr No. </th>
                                        <th>Make</th>
                                        <th>Model</th>
                                        <th>Name</th>
                                        <th>Cubic Capacity</th>
                                        <th>Seating Capacity</th>
                                        <th>Fuel Type</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($vwvariants as $key => $vwvariant)
                                    <tr>
                                        <td>{{ $key + $vwvariants->firstItem() }}.</td>
                                        <td>{{ @$vwvariant-> model-> make ->name }}</td>
                                        <td>{{ @$vwvariant-> model ->name }}</td>
                                        <td>{{ $vwvariant->name }}</td>
                                        <td>{{ $vwvariant->cubic_capacity }}</td>
                                        <td>{{ $vwvariant->seating_capacity }}</td>
                                        <td>{{ @$vwvariant->fuel_type->name }}</td>

                                        <td>
                                            <button class="btn tblActnBtn">
                                                <a href="{{ route('admin.variant.edit',$vwvariant->id) }}" style="color: black;"><i class="material-icons">mode_edit</i></a>
                                            </button>
                                            {{ Form::open(array('url' => route('admin.variant.destroy',$vwvariant->id), 'class' => 'btn delete-form tblActnBtn')) }}
                                            {{ Form::hidden('_method', 'DELETE') }}
                                            <button type="button" class="btn delete-btn tblActnBtn">
                                                <a style="color: black;"><i class="material-icons">delete</i></a>
                                            </button>
                                            {{ Form::close() }}
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div>
                            {{ $vwvariants->appends(request()->except('page'))->links('pagination::bootstrap-4')  }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>