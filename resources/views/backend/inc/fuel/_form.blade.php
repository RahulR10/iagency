@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">


  <div class="col-lg-6">
    {{Form::text('name', '', ['class' => 'squareInput', 'placeholder'=>'Enter Fule Type','required'=>'required'])}}
    {{Form::label('name', 'Enter Fule Type'), ['class' => 'active']}}
  </div>

  {{-- <div class="col-lg-6">
    {{ Form::text('slug','',['class' => 'squareInput','placeholder'=>'slug']) }}
    {{ Form::label('slug','Slug') }}
  </div> --}}
 
</div>