@if($message = Session::get('error'))
<div class="alert alert-danger alert-block">
  <button type="button" class="close" data-dismiss="alert">x</button>
  {{$message}}
</div>
@endif

@if(count($errors->all()))
<div class="alert alert-danger">
  <ul>
    @foreach($errors->all() as $error)
    <li>{{$error}}</li>
    @endforeach
  </ul>
</div>
@endif
<div class="row">
  <div class="col-lg-4">
    <div class="form-group">
      {{ Form::label('category', 'Select Category'), ['class' => 'active'] }}
      {{ Form::select('record[type_id]', $categoryArr,'0', ['class'=>'form-control', 'style'=> 'border:1px solid;','required'=>'required', 'id' => 'category_id']) }}
    </div>
  </div>
  @if (!in_array($category->slug, [
        'gcv-3w-public',
        'gcv-3w-private',
        'gcv-other-then-3w-private',
        'gcv-other-then-3w-public',
        'miscd',
        '3w-below-6-seats',
        '3w-above6-seats',
        'school-bus',
        'school-bus-electric',
        'tourist-bus',
        'tourist-bus-electric',
        '3w-below-6-seats-electric',
        '3w-above6-seats-electric',
        'gcv-3w-public-electric',
        'gcv-3w-private-electric',
    ]))
  <div class="col-lg-4">
    <div class="form-group" id="ccSelect">
      @include('backend.template.cc_select', compact('ccArr'))
    </div>
  </div>
  @endif
  @if (!in_array($category->slug, [
        'gcv-3w-public',
        'gcv-3w-private',
        'gcv-other-then-3w-private',
        'gcv-other-then-3w-public',
        'miscd',
        '3w-below-6-seats',
        '3w-above6-seats',
        'school-bus',
        'school-bus-electric',
        'tourist-bus',
        'tourist-bus-electric',
        'texi',
        '2w',
        '3w-below-6-seats-electric',
        '3w-above6-seats-electric',
        'gcv-3w-public-electric',
        'gcv-3w-private-electric',
    ]))
  <div class="col-lg-4">
    <div class="form-group">
      {{ Form::label('record[type]', 'Select Type'), ['class' => 'active'] }}
      {{ Form::select('record[type]', $typeArr,'0', ['class'=>'form-control', 'style'=> 'border:1px solid;','required'=>'required']) }}
    </div>
  </div>
  @endif
  @if ($category->slug == 'gcv-other-then-3w-private' || $category->slug == 'gcv-other-then-3w-public')
  <div class="col-lg-4">
    <div class="form-group">
      {{ Form::label('record[wc]', 'Select Weight Capacity'), ['class' => 'active'] }}
      {{ Form::select('record[wc]', $weightArr,'0', ['class'=>'form-control', 'style'=> 'border:1px solid;','required'=>'required']) }}
    </div>
  </div>
  @endif
  @if (
    in_array($category->slug, [
        '3w-below-6-seats',
        '3w-above6-seats',
        'school-bus',
        'school-bus-electric',
        'tourist-bus',
        'tourist-bus-electric',
        'texi',
        '2w',
        '3w-below-6-seats-electric',
        '3w-above6-seats-electric',
    ])
  )
  <div class="col-lg-4">
    <div class="form-group">
      {{ Form::label('per_pessanger', 'Enter Per Pessanger Price'), ['class' => 'active'] }}
      {{ Form::text('record[per_pessanger]','', ['class'=>'form-control', 'placeholder'=>'Enter Per Pessanger Price']) }}
    </div>
  </div>
  @endif
  <div class="col-lg-12">
    <div class="form-group">
      {{ Form::label('price', 'Enter Price'), ['class' => 'active'] }}
      {{ Form::text('record[price]','', ['class'=>'form-control', 'placeholder'=>'Enter Price']) }}
    </div>
  </div>
</div>