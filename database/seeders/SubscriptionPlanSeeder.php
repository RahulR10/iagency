<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class SubscriptionPlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $plan = new \App\Models\SubscriptionPlan();
        $plan->name = 'Silver';
        $plan->amount = 999;
        $plan->description = 'Some Text';
        $plan->duration_type = 'year';
        $plan->duration = 1;
        $plan->plan_level = 1;
        $plan->save();

        $plan = new \App\Models\SubscriptionPlan();
        $plan->name = 'Gold';
        $plan->amount = 1299;
        $plan->description = 'Some Text';
        $plan->duration_type = 'year';
        $plan->duration = 1;
        $plan->plan_level = 2;
        $plan->save();
    }
}
