<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddFieldsToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {

            // $table->dropForeign(['subscription_plan_id']);
            // $table->dropColumn('subscription_plan_id');
            // $table->dropColumn('valid_upto');
            // $table->dropColumn('deleted_at');

            $table->unsignedBigInteger('subscription_plan_id')->nullable()->after('remember_token');
            $table->foreign('subscription_plan_id')->references('id')->on('subscription_plans')->onDelete('SET NULL');
            $table->date('valid_upto')->nullable()->after('subscription_plan_id');
            $table->softDeletes()->after('valid_upto');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['subscription_plan_id']);
            $table->dropColumn(['subscription_plan_id']);
            $table->dropColumn(['valid_upto']);
        });
    }
}
