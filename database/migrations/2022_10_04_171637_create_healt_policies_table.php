<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHealtPoliciesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('healt_policies', function (Blueprint $table) {
            $table->id();
            $table->enum('health_policy_type',['Basic','TopUp','Personal Accidental','Critical'])->default('Basic');
            $table->enum('policy_type',['Individual','Family','Multi Individual'])->default('Individual');
            $table->enum('family_size_adult',['1','2'])->default('1');
            $table->enum('family_size_child',['1','2','3','4','5','6'])->default('1');
            $table->enum('zone',['A','B','C'])->default('A');
            $table->double('sum_assured');
            $table->enum('policy_tenner',['1','2','3']);
            $table->unsignedBigInteger('isurer_id')->nullable();
            $table->foreign('isurer_id')->references('id')->on('companies')->onDelete('SET NULL');
            $table->unsignedBigInteger('broker_id')->nullable();
            $table->foreign('broker_id')->references('id')->on('agents')->onDelete('cascade');

            $table->unsignedBigInteger('sub_agent_id')->nullable();
            $table->foreign('sub_agent_id')->references('id')->on('agents')->onDelete('cascade');
            $table->string('plain_name')->nullable();
            $table->string('policy_number');
            $table->date('policy_start_date');
            $table->date('policy_end_date');
            $table->double('premium_basic');
            $table->float('gst');
            $table->float('total');
            $table->string('customer_name');
            $table->string('customer_mobile');
            $table->string('email');
            $table->date('customer_dob');
            $table->float('commission_in_percent')->nullable();
            $table->float('commission_in_value')->nullable();
            $table->float('commission_out_percent')->nullable();
            $table->float('commission_out_value')->nullable();
            $table->float('pandl_comm_in_cmm_out')->nullable();
            $table->string('docs')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('healt_policies');
    }
}
