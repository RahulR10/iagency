<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVmastersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('vmasters', function (Blueprint $table) {
        //     $table->id();
        //     $table->foreignId('type_id')->nullable()->constrained()->onDelete('SET NULL');
        //     $table->foreignId('make_id')->nullable()->constrained()->onDelete('SET NULL');
        //     $table->foreignId('vwmodel_id')->nullable()->constrained()->onDelete('SET NULL');
        //     $table->foreignId('vwvariant_id')->nullable()->constrained()->onDelete('SET NULL');
        //     $table->softDeletes();
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vmasters');
    }
}
