<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBusinessFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_forms', function (Blueprint $table) {
            $table->id();
            $table->string('vehicle_type')->nullable();

            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('SET NULL');

            $table->unsignedBigInteger('vehicle_category_id')->nullable();
            $table->foreign('vehicle_category_id')->references('id')->on('types')->onDelete('SET NULL');
            $table->unsignedBigInteger('vehicle_subcategory_id')->nullable();
            $table->foreign('vehicle_subcategory_id')->references('id')->on('types')->onDelete('SET NULL');
            $table->string('vehicle_policy_type')->nullable();
            $table->string('carrier')->nullable();
            $table->unsignedBiginteger('vwvariant_id')->nullable();
            $table->foreign('vwvariant_id')->references('id')->on('vwvariant')->onDelete('SET NULL');
            $table->string('cubic_capacity')->nullable();
            $table->string('seating_capacity')->nullable();
            $table->unsignedBigInteger('rto_id')->nullable();
            $table->foreign('rto_id')->references('id')->on('rtos')->onDelete('cascade');

            $table->string('vehicle_number')->nullable();
            $table->string('zone')->nullable();
            $table->date('reg_date')->nullable()->comment('format yyyy-mm-dd');
            $table->string('engine_number')->nullable();
            $table->string('chassis_number')->nullable();
            $table->enum('is_prev_policy', ['Y', 'N'])->nullable()->comment('Y/N');
            $table->string('prev_policy_type')->nullable();
            $table->date('prev_policy_exp_date')->nullable()->comment('format yyyy-mm-dd');

            $table->unsignedBigInteger('prev_isurer_id')->nullable();
            $table->foreign('prev_isurer_id')->references('id')->on('companies')->onDelete('SET NULL');
            $table->string('prev_policy_number')->nullable();
            $table->enum('prev_policy_claim', ['Y', 'N'])->nullable();
            $table->integer('prev_ncb')->nullable();
            
            $table->unsignedBigInteger('isurer_id')->nullable();
            $table->foreign('isurer_id')->references('id')->on('companies')->onDelete('SET NULL');
            $table->string('policy_number')->nullable();
            $table->date('policy_start_date')->nullable()->comment('format yyyy-mm-dd');
            $table->date('policy_end_date')->nullable()->comment('format yyyy-mm-dd');
            $table->string('idv')->nullable();
            $table->integer('current_ncb')->nullable();
            $table->text('addons')->nullable();
            $table->enum('is_financed', ['Y', 'N'])->nullable();
            $table->string('finance_company_name')->nullable();
            $table->double('od_premium')->nullable();
            $table->double('tp_premium')->nullable();
            $table->double('net_premium')->nullable()->comment('OD+TP');
            $table->double('gst_cess')->nullable();
            $table->double('final_premium')->nullable()->comment('NET+GST+CESS');
            $table->enum('customer_type', ['Individual', 'Company'])->nullable()->comment('NET+GST+CESS');

            $table->enum('title', ['Mr.', 'Mrs.', 'Ms.'])->nullable();
            $table->string('full_name')->nullable();
            $table->string('mobile', 20)->nullable();
            $table->string('email')->nullable();
            $table->text('address')->nullable();
            $table->string('pincode', 10)->nullable();
            $table->string('city')->nullable();
            $table->string('state')->nullable();

            $table->unsignedBigInteger('broker_id')->nullable();
            $table->foreign('broker_id')->references('id')->on('agents')->onDelete('cascade');

            $table->unsignedBigInteger('sub_agent_id')->nullable();
            $table->foreign('sub_agent_id')->references('id')->on('agents')->onDelete('cascade');

            $table->enum('commission_receivable', ['Flat', 'Percent'])->nullable();
            $table->double('commission_receivable_amount')->nullable();
            $table->double('cr_od_premium')->nullable();
            $table->double('cr_tp_premium')->nullable();
            $table->double('cr_net_premium')->nullable();
            $table->double('cr_total')->nullable();

            $table->enum('commission_payable', ['Flat', 'Percent'])->nullable();
            $table->double('commission_payable_amount')->nullable();
            $table->double('cp_od_premium')->nullable();
            $table->double('cp_tp_premium')->nullable();
            $table->double('cp_net_premium')->nullable();
            $table->double('cp_total')->nullable();

            $table->string('policy_pdf')->nullable();

            $table->softDeletes();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_forms');
    }
}
