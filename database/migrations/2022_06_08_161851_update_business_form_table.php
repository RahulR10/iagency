<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateBusinessFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('business_forms', function (Blueprint $table) {
            $table->double('payout_in')->nullable();
            $table->enum('payout_in_status', ['pending', 'received'])->default('pending');
            $table->double('payout_out')->nullable();
            $table->enum('payout_out_status', ['pending', 'paid'])->default('pending');
            $table->double('pnl')->comment('p & l')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('business_forms', function (Blueprint $table) {
            //
        });
    }
}
