<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Models\Type;
use App\Models\Make;
use App\Models\Vwmodel;
use App\Models\VwfuelType;
use App\Models\Vwvariant;

class VehicleTypeController extends Controller
{
    public function index()
    {
        $lists = Type::with(['subcategories' => function ($q) {
            return $q->where('type_for', 'business-form');
        }])->whereNull('type_id')->select('id', 'name', 'icon')->get();

        $re = [
            'status'    => true,
            'message'   => $lists->count() . ' record(s) found.',
            'data'      => $lists,
        ];

        return response()->json($re);
    }

    public function make($vehicle_type_id)
    {
        $lists = Make::where('type_id', $vehicle_type_id)->orderBy('name')->get();
        // dd($vehicle_type_id);
        $re = [
            'status'    => true,
            'message'   => $lists->count() . ' record(s) found.',
            'data'      => $lists,
        ];

        return response()->json($re);
    }

    public function model($make_id)
    {
        $lists = Vwmodel::where('make_id', $make_id)->orderBy('name')->get();

        $re = [
            'status'    => true,
            'message'   => $lists->count() . ' record(s) found.',
            'data'      => $lists,
        ];

        return response()->json($re);
    }

    public function fuel()
    {
        $lists = VwfuelType::orderBy('name')->get();

        $re = [
            'status'    => true,
            'message'   => $lists->count() . ' record(s) found.',
            'data'      => $lists,
        ];

        return response()->json($re);
    }

    public function variant($model_id, $fuel_id)
    {
        $lists = Vwvariant::where('vwmodel_id', $model_id)->where('vwmfuel_type_id',$fuel_id)->orderBy('name')->get();

        $re = [
            'status'    => true,
            'message'   => $lists->count() . ' record(s) found.',
            'data'      => $lists,
        ];

        return response()->json($re);
    }
}
