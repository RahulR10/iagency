<?php

namespace App\Http\Controllers\Api\Rtoapp;

use Hash;
use App\Models\Signature;
use App\Models\User;
use App\Models\Setting;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function sendotp(Request $request)
    {
        $mobile = $request->mobile;

        $setting = Setting::find(1);
        $input  = $request->all();
        $user = User::where('mobile',$input['mobile']);

        if ($user->count()) {
            $user = $user->first();
            $otp = rand(100000, 999999);
            $user->password=\Hash::make($otp);

            $msg    = urlencode("Dear". $setting->title. "user, ". $otp . "  is the otp of your mobile verfication. PLS DO NOT SHARE WITH ANYONE.
            Regards
            IAGENCY");
            $user->otp = $otp;
            $user->save();

            $re = [
                'status'    => true,
                'message'   => 'Success! Otp Send Successfully',
                'data'      => $user
            ];

        } else {
            $user = new User();
            $user->mobile = $request->mobile;
            $otp = rand(100000, 999999);
            $user->password=\Hash::make($otp);
            $user->role_id = 2;
            $user->save();
            
            $re = [
                'status'    => true,
                'message'   => 'Success! Otp Send Successfully',
                'data'      => $user
            ];
        };

        $msg    = urlencode("Dear " . $setting->title . " user, " . $otp . " is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE.Regards IAGENCY");
        $apiUrl = str_replace(["[MESSAGE]", "[MOBILE]"], [$msg, request('mobile')], $setting->sms_api);
        $sms    = file_get_contents($apiUrl);

        $re['sms'] = $sms;

        return response()->json($re);
    }


    public function verifyotp(Request $request)
    {
        $request->validate([
            'mobile'   => 'required',
            'otp'      => 'required',
        ]);

        $cred = [
            'mobile'    => $request-> mobile,
            'password'  =>  $request->otp,
        ];

        $check = \Auth::attempt($cred);

        if ($check) {
            $user = \Auth::user();
            // $user->api_token =\Str::random(60);
            // $user->save();

            $token = $user->createToken('RTO-App')->accessToken;
            // $user->api_token = null;            
            $user->token = $token;
            $user->save();

            return response()->json([
                'user'  => $user, 
                'message' => 'Success! OTP verified.',
                'is_exists' => $user->is_exists
            ]);
        } else {
            return response()->json(['message' => 'Wrong otp has been entered.'],403);
        } 

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user = auth()->user();

        $user->name           = $request->name;
        $user->email          = $request->email;
        // $user->mobile         = $request->mobile;
        // $user->password       = Hash::make(request('password'));
        $user->address_id     = $request->address_id;
        $user->device_type    = $request->device_type;
        $user->fcm_id         = $request->fcm_id;
        $user->termcondition  = $request->termcondition;
        $user->accept_code    = $request->accept_code;
        $user->role_id        ='2';
        $user->pincode        = $request->pincode;
        $user->referal_code   = $request->referal_code;
        $user->is_exists      = "Y";

        $name = str_replace(' ', '', $request->name);
        $name = ucwords(strtoupper($name));
        $name = substr($name, 0, 4);
        $referal_code = trim($name . '' . $this->referalcode());

        $setting = Setting::find(1);
        $user->referal_code = $referal_code;

        if ($request->accept_code != '') {
            $checkCode = User::where('referal_code', $request->accept_code)->get();

            if (count($checkCode) == 1) {

                //Generate Otp
                $otp        = rand(100000, 999999);
                

                $user->accept_code = $request->accept_code;
                $user->otp = $otp;
                $user->valid_upto = date("Y-m-d", strtotime("+".$setting->trial_duration . ' days'));
                $user->save();

                //add signnature
                $signnature = new Signature();
                $signnature->user_id = $user->id;
                $signnature->name = $user->name;
                $signnature->email = $user->email;
                $signnature->mobile = $user->mobile;
                $signnature->save();

                $re = [
                    'status'        => true,
                    'message'       => 'Success!! Registration successful.',
                    'user'          => $user,
                    'signnature'    => $signnature,
                ];
            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Error!! Referal code not valid.',
                ];
            }
        } else {
            $user->valid_upto = date("Y-m-d", strtotime("+".$setting->trial_duration . ' days'));
            $user->save();

            //add signnature
            $signnature = new Signature();
            $signnature->user_id = $user->id;
            $signnature->name = $user->name;
            $signnature->email = $user->email;
            $signnature->mobile = $user->mobile;
            $signnature->save();

            $re = [
                'status'        => true,
                'message'       => 'Success!! Registration successful.',
                'user'          => $user,
                'signnature'    => $signnature,
            ];
        }

        $user->save();

        return response()->json($re);
    }

    protected function referalcode($length = 6)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
