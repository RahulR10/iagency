<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Agent;
use Illuminate\Http\Request;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = auth()->user();
        $agents = Agent::where('user_id', $user->id);
        
        if(!empty($request->type))
        {
            $agents->where('type', $request->type);
        }

$agents = $agents->select('name', 'id')->get();
        return $re = [
            'status'    => true,
            'message'   => $agents->count() . ' record(s) found.',
            'data'      => $agents
        ];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name'  => 'required',
            'type'  => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            if($request->type != 'broker' && $request->type != 'subagent') {
                $re = [
                    'status'    => false,
                    'message'   => 'Validations errors found.',
                    'errors'    => 'type is either broker or subagent'
                ];
            } else {
                $user = auth()->user();
                $agent = Agent::firstOrCreate([
                        'name'      => trim($request->name),
                        'type'      => $request->type,
                        'user_id'   => $user->id
                    ]);

                $agents = Agent::where('user_id', $user->id)
                    ->where('type', $request->type)
                    ->pluck('name', 'id');

                return $re = [
                    'status'    => true,
                    'message'   => $request->type . ' has been added.',
                    'data'      => $agents
                ];
            }
        }

        $user = auth()->user();
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function show(Agent $agent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agent $agent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agent  $agent
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agent $agent)
    {
        //
    }
}
