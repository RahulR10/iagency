<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\SeatCapacity;
use Illuminate\Http\Request;
use Validator;

class SeatCapacityController extends Controller
{
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type_id'  => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $lists = SeatCapacity::get();

            if ($lists->isEmpty()) {
                $re = [
                    'status' => false,
                    'message'    => 'No record(s) found.'
                ];
            } else {
                $re = [
                    'status' => true,
                    'message'    => $lists->count() . " records found.",
                    'data'   => $lists
                ];
            }
        }
        return response()->json($re);
    }
}
