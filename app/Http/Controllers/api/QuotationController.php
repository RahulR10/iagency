<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Quotation;
use App\Models\Signature;
use App\Models\Company;
use App\Models\User;
use Validator;
use Pdf;

class QuotationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();
        $lists = Quotation::where('user_id', $user->id)->latest()->get();
        foreach ($lists as $list) {
            $pdf_details = json_decode($list->data);
            $list->data = json_decode($list->data);
            // $list->pdf_url = url('api/quotation/pdf/' . $list->id);
            $list->pdf_url = $list->pdf ? url('pdf/' . $list->pdf) : '';
            // dd($pdf_details);
            if(!empty($pdf_details->customer_detail)) :
                
                $list->customer_detail = $pdf_details->customer_detail;
                if (!empty($pdf_details->customer_detail->finance_company)) {
                    $company = Company::where('id', $pdf_details->customer_detail->finance_company)->first();
                    $list->finance_company = $company;
                }
            
            endif;


            unset($list->data);
        }

        if ($lists->isEmpty()) {
            $re = [
                'status'    => false,
                'message'   => 'No record(s) found.'
            ];
        } else {
            $re = [
                'status'    => true,
                'message'   => $lists->count() . " records found.",
                'data'      => $lists
            ];
        }
        return response()->json($re);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'data'      => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $user = auth()->user();
            $quotation = new Quotation;
            $quotation->user_id = $user->id;
            // $data = [
            //     'basic_details'         => [
            //         'type_of_vehivle'                   => 'New',
            //         'policy_type'                       => '1+5 boundle policy',
            //         'trto'                              => 'Jodhpur',
            //         'zone'                              => 'B',
            //         'cubic_capacity'                    => 'UPTO 75 CC',
            //         'age_of_vehicle'                    => '0 Years',
            //     ],
            //     'part_a_own_damage'     => [
            //         'vehicle_basic_rate'                => '1.676',
            //         'basic_own_damage'                  => '1.676',
            //         'discount'                          => '1.676',
            //         'loading'                           => '1.676',
            //         'basic_od_after_discount'           => '1.676',
            //         'electric_accessories_value'        => '1.676',
            //         'non_electric_accessories_value'    => '1.676',
            //         'basic_od_before_ncb'               => '1.676',
            //         'no_claim_bonus'                    => '1.676',
            //         'gross_own_damage'                  => '1.676',
            //         'zero_dep'                          => '1.676',
            //         'other_add_on_percent'              => '1.676',
            //         'other_add_on'                      => '1.676',
            //         'final_own_damage'                  => '1.676',
            //     ],
            //     'part_b_liability'      => [
            //         'basic_tp'                          => '1.676',
            //         'cpa_owner_driver'                  => '1.676',
            //         'll_to_paid_driver'                 => '1.676',
            //         'pa_to_unnamed_passanger'           => '1.676',
            //         'tppd_cover'                        => '1.676',
            //         'final_tp'                          => '1.676',
            //     ],
            //     'part_c_final_premium'  => [
            //         'premium_before_taxes'              => '1.676',
            //         'gst'                               => '1.676',
            //         'kerala_cess'                       => '1.676',
            //         'final_premium'                     => '1.676',
            //     ]
            // ];
            // $data = {"basic_details":{"type_of_vehivle":"NEW","policy_type":"1+5 BUNDLE POLICY","trto":"Jodhpur","zone":"B","cubic_capacity":"76-150 CC","age_of_vehicle":"0 Years"},"part_a_own_damage":{"idv":"10000","vehicle_basic_rate":"1.676","basic_own_damage":"167.6","discount":"0.0","loading":"0","basic_od_after_discount":"167.6","electric_accessories_value":"0.0","non_electric_accessories_value":"0.0","basic_od_before_ncb":"167.6","no_claim_bonus":"0.0","gross_own_damage":"167.6","zero_dep":"0.0","other_add_on_percent":"0.0","other_add_on":"0","final_own_damage":"167.6"},"part_b_liability":{"basic_tp":"3285","cpa_owner_driver":"0","ll_to_paid_driver":"0","pa_to_unnamed_passanger":"0","tppd_cover":"0","final_tp":"3285.0"},"part_c_final_premium":{"premium_before_taxes":"3452.6","gst":"621.468","kerala_cess":"0.0","final_premium":"4074.068"},"customer_detail":{"customer_name":"Ankit","customer_mobile":"9632587422","customer_vehicle":"rj19sl8806","finance_company":"1"}};
            // dd($data);
            $quotation->data = $request->data;
            $quotation->type_id = $request->type_id;
            $quotation->save();
            
            $quotation = Quotation::with('category')->findOrFail($quotation->id);
            $pdf_details = json_decode($quotation->data);
            $quotation->data = json_decode($quotation->data);
            $quotation->date =  date("d-m-Y", strtotime($quotation->created_at));
            $quotation->company = Company::where('id', $pdf_details->customer_detail->finance_company)->first();
            $user = auth()->user();
            $user->sign = Signature::where('user_id', $user->id)->first();
            $data = compact('quotation', 'user');
            // dd($data);
            $filename = 'pdf' . rand(1000, 9999).'.pdf';
            $pdf = Pdf::loadView('quotation', $data);
            $filepath = public_path() . '/pdf/'.$filename;
            
            $quotation_update = Quotation::find($quotation->id);
            $quotation_update->pdf = $filename;
            $quotation_update->save();
            // dd($filepath);
            $pdf->save($filepath);
            $quotation->pdf = url('pdf/' . $filename);
            $re = [
                'status'        => true,
                'message'       => 'Success!! Quotation added successfully.',
                'quotation'     => $quotation,
            ];
        }

        return response()->json($re);
    }

    public function quotation_pdf(Request $request, $id)
    {
        if(auth()->check()) $user = auth()->user();
        else {
            return response()->json([
                'msg' => 'Forbidden Error'
            ], 401);
        };
        
        $user->sign = Signature::where('user_id', $user->id)->first();
        $quotation = Quotation::with('category')->findOrFail($id);
        $pdf_details = json_decode($quotation->data);
        $quotation->data = json_decode($quotation->data);
        $quotation->date =  date("d-m-Y", strtotime($quotation->created_at));
        $quotation->company = Company::where('id', $pdf_details->customer_detail->finance_company)->first();
        $data = compact('quotation', 'user');
        // dd($data);
        $filename = 'pdf' . rand(1000, 9999).'.pdf';
        $pdf = Pdf::loadView('quotation', $data);
        $filepath = public_path() . '/pdf/'.$filename;
        
        $quotation_update = Quotation::find($id);
        $quotation_update->pdf = $filename;
        $quotation_update->save();
        // dd($filepath);
        $pdf->save($filepath);
        return $pdf->stream("quotation.pdf");
    }
    
    public function quotation_pdf2(Request $request, $token)
    {
        if(auth()->check()) $user = auth()->user();
        else $user = User::find(21);
        
        // $token = 
        
        $user->sign = Signature::where('user_id', $user->id)->first();
        $quotation = Quotation::with('category')->findOrFail($id);
        $pdf_details = json_decode($quotation->data);
        $quotation->data = json_decode($quotation->data);
        $quotation->date =  date("d-m-Y", strtotime($quotation->created_at));
        $quotation->company = Company::where('id', $pdf_details->customer_detail->finance_company)->first();
        $data = compact('quotation', 'user');
        // dd($data);
        $filename = 'pdf' . rand(1000, 9999).'.pdf';
        $pdf = Pdf::loadView('quotation', $data);
        $filepath = public_path() . '/pdf/'.$filename;
        
        $quotation_update = Quotation::find($id);
        $quotation_update->pdf = $filename;
        $quotation_update->save();
        // dd($filepath);
        $pdf->save($filepath);
        return $pdf->stream("quotation.pdf");
    }

    public function destroy(Quotation $quotation)
    {
        $quotation->Delete();
        return response()->json([
            'message' => 'Quotation has been deleted',
            'data'    => $quotation  
        ]);
    }
}
