<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\HealtPolicy;
use Dflydev\DotAccessData\Data;
use Illuminate\Http\Request;

class HealtPolicyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = HealtPolicy::get();

        if(!empty($request->isurer_id)) {
            $isurerArr = explode(",", $request->isurer_id);
            $query->whereIn('isurer_id', $isurerArr);
        }
        
        if(!empty($request->broker_id)) {
            $brokerArr = explode(",", $request->broker_id);
            $query->whereIn('broker_id', $brokerArr);
        }
        
        if(!empty($request->sub_agent_id)) {
            $subAgentArr = explode(",", $request->sub_agent_id);
            $query->whereIn('sub_agent_id', $subAgentArr);
        }

        return response()->json($query);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $healtPolicy = new HealtPolicy();

        $healtPolicy->health_policy_type    = $request->health_policy_type;
        $healtPolicy->policy_type           = $request->policy_type;
        $healtPolicy->family_size_adult     = $request->family_size_adult;  
        $healtPolicy->family_size_child     = $request->family_size_child;  
        $healtPolicy->zone                  = $request->zone;  
        $healtPolicy->sum_assured           = $request->sum_assured;
        $healtPolicy->policy_tenner         = $request->policy_tenner;
        $healtPolicy->isurer_id             = $request->isurer_id;
        $healtPolicy->broker_id             = $request->broker_id;
        $healtPolicy->sub_agent_id          = $request->sub_agent_id;
        $healtPolicy->plain_name            = $request->plain_name;
        $healtPolicy->policy_number         = $request->policy_number;
        $healtPolicy->policy_start_date     = $request->policy_start_date;
        $healtPolicy->policy_end_date       = $request->policy_end_date;
        $healtPolicy->premium_basic         = $request->premium_basic;
        $healtPolicy->gst                   = $request-> gst/100;
        $healtPolicy->total                 = $request->gst * $request->premium_basic;
        $healtPolicy->customer_name         = $request->customer_name;
        $healtPolicy->customer_mobile       = $request->customer_mobile;
        $healtPolicy->email                 = $request->email;
        $healtPolicy->customer_dob          = $request->customer_dob;
        $healtPolicy->commission_in_percent = $request->commission_in_percent/100;
        $healtPolicy->commission_in_value   = $request->premium_basic * $request->commission_in_percent;
        
        $healtPolicy->commission_out_percent= $request->commission_out_percent / 100;
        $healtPolicy->commission_out_value  = $request->premium_basic * $request->commission_out_percent;

        $healtPolicy->pandl_comm_in_cmm_out = $request->commission_in_value - $request->commission_out_value;

        $healtPolicy->save();

        return response()->json([
            'data'    => $healtPolicy,
            'message' => 'Data has been added.'
        ],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HealtPolicy  $healtPolicy
     * @return \Illuminate\Http\Response
     */
    public function show(HealtPolicy $healtPolicy)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HealtPolicy  $healtPolicy
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, HealtPolicy $healtPolicy)
    {
        
        $healtPolicy->health_policy_type    = $request->health_policy_type;
        $healtPolicy->policy_type           = $request->policy_type;
        $healtPolicy->family_size_adult     = $request->family_size_adult;  
        $healtPolicy->family_size_child     = $request->family_size_child;  
        $healtPolicy->zone                  = $request->zone;  
        $healtPolicy->sum_assured           = $request->sum_assured;
        $healtPolicy->policy_tenner         = $request->policy_tenner;
        $healtPolicy->isurer_id             = $request->isurer_id;
        $healtPolicy->broker_id             = $request->broker_id;
        $healtPolicy->sub_agent_id          = $request->sub_agent_id;
        $healtPolicy->plain_name            = $request->plain_name;
        $healtPolicy->policy_number         = $request->policy_number;
        $healtPolicy->policy_start_date     = $request->policy_start_date;
        $healtPolicy->policy_end_date       = $request->policy_end_date;
        $healtPolicy->premium_basic         = $request->premium_basic;
        $healtPolicy->gst                   = $request-> gst/100;
        $healtPolicy->total                 = $request->gst * $request->premium_basic;
        $healtPolicy->customer_name         = $request->customer_name;
        $healtPolicy->customer_mobile       = $request->customer_mobile;
        $healtPolicy->email                 = $request->email;
        $healtPolicy->customer_dob          = $request->customer_dob;
        $healtPolicy->commission_in_percent = $request->commission_in_percent/100;
        $healtPolicy->commission_in_value   = $request->premium_basic * $request->commission_in_percent;
        
        $healtPolicy->commission_out_percent= $request->commission_out_percent / 100;
        $healtPolicy->commission_out_value  = $request->premium_basic * $request->commission_out_percent;

        $healtPolicy->pandl_comm_in_cmm_out = $request->commission_in_value - $request->commission_out_value;

        $healtPolicy->save();

        return response()->json([
            'data'    => $healtPolicy,
            'message' => 'Data has been Updated.'
        ],500);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HealtPolicy  $healtPolicy
     * @return \Illuminate\Http\Response
     */
    public function destroy(HealtPolicy $healtPolicy)
    {
        $healtPolicy->delete();

        return response()->json([
            'message'   =>  'Data has been deleted',
        ]);
    }
}
