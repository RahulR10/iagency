<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\BusinessForm;
use Illuminate\Http\Request;

class BusinessFormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->limit ?: 100;
        $query = BusinessForm::where('user_id', auth()->user()->id);

        if(!empty($request->type) && $request->type == 'renewel') {
            $query->whereNotNull('policy_end_date')->where('policy_end_date', '<=', date('Y-m-d', strtotime("+2 months")));
        }
        
        // Filter
        if(!empty($request->vehicle_policy_type)) {
            $policyTypeArr = explode(",", $request->vehicle_policy_type);
            $query->whereIn('vehicle_policy_type', $policyTypeArr);
        }
        if(!empty($request->vehicle_category_id)) {
            $vehicleTypeArr = explode(",", $request->vehicle_category_id);
            $query->whereIn('vehicle_category_id', $vehicleTypeArr);
        }
        // return response()->json([$request->from_date, $request->to_date]);
        if(!empty($request->from_date)) {
            $query->where('policy_start_date', '>=', $request->from_date);
        }
        
        if(!empty($request->to_date)) {
            $query->where('policy_start_date', '<=', $request->to_date);
        }
        
        if(!empty($request->isurer_id)) {
            $isurerArr = explode(",", $request->isurer_id);
            $query->whereIn('isurer_id', $isurerArr);
        }
        
        if(!empty($request->broker_id)) {
            $brokerArr = explode(",", $request->broker_id);
            $query->whereIn('broker_id', $brokerArr);
        }
        
        if(!empty($request->sub_agent_id)) {
            $subAgentArr = explode(",", $request->sub_agent_id);
            $query->whereIn('sub_agent_id', $subAgentArr);
        }
        
        // Search
        if(!empty($request->full_name)) {
            $query->where('full_name', 'LIKE', $request->full_name.'%');
            
        }
        if(!empty($request->mobile)) {
            $query->where('mobile', 'LIKE', $request->mobile.'%');
            
        }
        if(!empty($request->vehicle_number)) {
            $vehicleNumber = str_replace(" ", "", $request->vehicle_number);
            $query->where('vehicle_number', 'LIKE', $vehicleNumber.'%');
        }
        if(!empty($request->policy_number)) {
            $query->where('policy_number', 'LIKE', $request->policy_number.'%');
        }
        
        if(!empty($request->search)) {
            $query->where('mobile', 'LIKE', $request->search.'%')
                ->orWhere('full_name', 'LIKE', $request->search.'%')
                ->orWhere('policy_number', 'LIKE', $request->search.'%')
                ->orWhere('vehicle_number', 'LIKE', $request->search.'%')
                ->orWhere('chassis_number', 'LIKE', $request->search.'%')
                ->orWhere('engine_number', 'LIKE', $request->search.'%');
        }

        $businessForms = $query->latest()->paginate($limit);
        
        $responseData = $businessForms->toArray();
        // return response()->json($responseData);
        // print_r($responseData); die;
        foreach($responseData['data'] as $key => $bForm) {
            $responseData['data'][$key]['vehicle_number'] = $bForm['vehicle_type'] == "NEW" ? "NEW" : $bForm['vehicle_number'];
        }
        
        ini_set('serialize_precision', '-1');

        $re = [
            'status'    => true,
            'message'   => $businessForms->total() . ' record(s) found.',
            'data'      => $responseData, // $businessForms->toArray(),
            'download_files_url' => [
                "pdf" => "https://freecontent.manning.com/wp-content/uploads/the-html-doctype.pdf",
                "csv" => "https://mesonet.agron.iastate.edu/data/csv/coop.csv",
            ]
        ];
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formData = $request->except(['policy_pdf']);
        $formData['vehicle_number'] = str_replace(" ", "", $formData['vehicle_number']);
        $businessForm = new BusinessForm($formData);
        $businessForm->user_id = auth()->user()->id;
        $businessForm->save();

        if($request->hasFile('policy_pdf'))
        {
            $desinationPath = public_path('uploads/business-form/');
            if(!is_dir($desinationPath) && !is_file($desinationPath)) {
                mkdir($desinationPath, 0755, true);
            }

            $file = $request->file('policy_pdf');
            $fileName = $businessForm->id . '.' . $file->getClientOriginalExtension();

            $file->move($desinationPath, $fileName);

            $businessForm->policy_pdf = $fileName . '?v=' . uniqid();
            $businessForm->save();
        }

        $re = [
            'status'    => true,
            'message'   => 'Business Form Added',
            'data'      => $businessForm,
            'response' => $request->all(),
        ];
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BusinessForm  $businessForm
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessForm $businessForm)
    {
        $re = [
            'status'    => true,
            'message'   => 'business form details.',
            'data'      => $businessForm,
        ];
        return response()->json($re);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BusinessForm  $businessForm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessForm $businessForm)
    {
        // dd($request->except(['policy_pdf']));
        // $businessForm->fill($request->except(['policy_pdf']));
        
        $formData = $request->except(['policy_pdf', '_method']);

        if(!empty($formData['vehicle_number']))
        $formData['vehicle_number'] = str_replace(" ", "", $formData['vehicle_number']);
        
        // $formData = json_decode($request->getContent(), true);
        
        foreach($formData as $field => $value) {
            $businessForm->{$field} = $value;
        }
        $businessForm->user_id = auth()->user()->id;
        $businessForm->save();

        if($request->hasFile('policy_pdf'))
        {
            $desinationPath = public_path('uploads/business-form/');
            if(!is_dir($desinationPath) && !is_file($desinationPath)) {
                mkdir($desinationPath, 0755, true);
            }

            $file = $request->file('policy_pdf');
            $fileName = $businessForm->id . '.' . $file->getClientOriginalExtension();

            $file->move($desinationPath, $fileName);

            $businessForm->policy_pdf = $fileName . '?v=' . uniqid();
            $businessForm->save();
        }

        $re = [
            'status'    => true,
            'message'   => 'Business Form Updated',
            'data'      => $businessForm,
            'response' => $request->all(),
        ];
        return response()->json($re);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BusinessForm  $businessForm
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessForm $businessForm)
    {
        $businessForm->delete();

        $re = [
            'status'    => true,
            'message'   => 'Business Form Removed',
        ];
        return response($re);
    }
}
