<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Media;
use Illuminate\Http\Request;

class MediaController extends Controller
{
    public function media()
    {
        $media = Media::get();

        return response()->json($media);
    }

    public function store(Request $request)
    {
        $media = New Media();

        $media->docs  = $request->file('docs')->store("uploads", ['disk' => 'public']);

        $media->save();

        return $media;
    }
}
