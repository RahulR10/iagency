<?php

namespace App\Http\Controllers\api;

use App\Models\SubscriptionPlan;
use App\Models\User;
use App\Models\Setting;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class SubscriptionPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subscriptionPlans = SubscriptionPlan::get();
        $res = [
            'status' => true,
            'data'   => $subscriptionPlans,
            'message' => $subscriptionPlans->count() . ' record(s) found.',
        ];
        return response()->json($res);
    }

    public function updateTrial()
    {
        $users = User::whereNull('valid_upto')->whereNull('subscription_plan_id')->get();
        $setting = Setting::whereRaw('1=1')->first();

        $duration = intval($setting->trial_duration);

        foreach ($users as $key => $user) {
            $user->valid_upto = date('Y-m-d', strtotime("+{$duration} months", strtotime($user->created_at)));
            $user->save();
        }

        $res = [
            'status'    => true,
            'message'   => "Total {$users->count()} rows affected.",
            'data'      => $user
        ];

        return response()->json($res);
    }

    public function updatePlan(Request $request)
    {
        $request->validate([
            'subscription_plan_id' => 'required|numeric'
        ]);

        $subscriptionPlan = SubscriptionPlan::findOrFail($request->subscription_plan_id);

        $user = auth()->user();
        $user->subscription_plan_id = $request->subscription_plan_id;
        $duration = intval($subscriptionPlan->duration);
        $user->valid_upto = date('Y-m-d', strtotime("+{$duration} {$subscriptionPlan->duration_type}s"));
        $user->save();

        return response([
            'status'    => true,
            'msg'       => 'Plan ' . $subscriptionPlan->name . ' has been activated.',
            'user'      => $user
        ]);
    }
}
