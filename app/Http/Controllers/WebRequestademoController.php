<?php

namespace App\Http\Controllers;

use App\Models\WebRequestademo;
use Illuminate\Http\Request;

class WebRequestademoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $requestademos = WebRequestademo::paginate(10);
        return view('backend.inc.wb_requestademo.list',['requestademo' => $requestademos]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WebRequestademo $webRequestademo)
    {
        
        $webRequestademo->web_product_id =$request->web_product_id;   
        $webRequestademo->name = $request->name;
        $webRequestademo->email = $request->email;
        $webRequestademo->mobile = $request->mobile;
        $webRequestademo->description = $request->description;

        $webRequestademo->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebRequestademo  $webRequestademo
     * @return \Illuminate\Http\Response
     */
    public function show(WebRequestademo $webRequestademo)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebRequestademo  $webRequestademo
     * @return \Illuminate\Http\Response
     */
    public function edit(WebRequestademo $webRequestademo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebRequestademo  $webRequestademo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebRequestademo $webRequestademo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebRequestademo  $webRequestademo
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebRequestademo $requestademo)
    {
        $requestademo->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
