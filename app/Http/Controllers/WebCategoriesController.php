<?php

namespace App\Http\Controllers;

use App\Models\WebCategories;
use Illuminate\Http\Request;

class WebCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = WebCategories::paginate(10);
        // dd($categories);
        return view('backend.inc.wb_category.list',['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request->flush();
        return view('backend.inc.wb_category.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WebCategories $categories)
    {
        $categories->title =$request->title;
        $categories->slug =$request->slug;
        $categories->save();
        
        return redirect(route('webadmin.category.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebCategories  $webCategories
     * @return \Illuminate\Http\Response
     */
    public function show(WebCategories $webCategories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebCategories  $webCategories
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,$webCategories)
    {
        $webCategories = WebCategories::findOrFail($webCategories);
        // dd($webCategories);
        $request->replace($webCategories->toArray());
        $request->flash();

        $data= compact('webCategories');
        return view('backend.inc.wb_category.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebCategories  $webCategories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebCategories $webCategories)
    {
        $categories->title =$request->title;
        $categories->slug =$request->slug;
        $categories->save();
        
        return redirect(route('webadmin.category.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebCategories  $webCategories
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebCategories $webCategories)
    {
        $webCategories->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
