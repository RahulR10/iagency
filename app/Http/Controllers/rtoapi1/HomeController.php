<?php

namespace App\Http\Controllers\rtoapi;

use App\Http\Controllers\Controller;
use Validator;
use App\Models\Vwuser;
use App\Models\Vwcategory;
use App\Models\Vwdocument;
use App\Models\Vwvehicledocument;
use App\Models\Vwvehicle;
use App\Models\Vwposter;
use App\Models\Vwslider;
use App\Models\Vwarticle;
use App\Models\Vwpage;
use App\Models\Vwotherapp;
use App\Models\Vwrenew;
use App\Models\Vwfeedback;
use Illuminate\Http\Request;
use DateTime;

class HomeController extends Controller
{
    public function home(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'language'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            // Category 
            $categories = Vwcategory::where('language',$request->language)->get();
            foreach($categories as $cat){
                $vehicle = Vwvehicle::where('category_id',$cat->id)->where('user_id',$request->user_id)->get();
                $cat->vehicle = $vehicle;
            }

            // Slider
            $sliders = Vwslider::latest()->get();

            // Doucument 
            $dt = new DateTime();
            $dt = $dt->format('Y-m-d');
            $documents = Vwdocument::where('language',$request->language)->get();
            foreach($documents as $doc){
                $vehicle_document = Vwvehicledocument::where('document_id',$doc->id)->whereHas('vehicle', function($q) use($request)
                {
                    $q->where('user_id','=', $request->user_id);
                
                })->where('expiry_date','<',$dt)->get();
                $doc->vehicle_document = $vehicle_document;
            }
            
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => [
                    'categories'  =>  $categories,
                    'sliders'    =>  $sliders,
                    'documents'  =>  $documents
                    ]
            ]; 
             
        }
        return response()->json($re);
    }

    public function category()
    {
        $list = Vwcategory::latest()->get();
        $re = [
            'status'     => true,
            'message'   => 'Success',
            'data'      => $list
        ];
        return response()->json($re);
    }

    public function document()
    {
        $list = Vwdocument::latest()->get();
        $re = [
            'status'     => true,
            'message'   => 'Success',
            'data'      => $list
        ];
        return response()->json($re);
    }

    public function poster_list()
    {
        
        $poster = Vwposter::get();
        // dd($poster);
        // foreach($poster as $poster)
        $re = [
            'status'    => true,
            'message'   => 'Success!',
            'data'      => $poster
        ];
        
        return response()->json($re);
    }

    public function article_list(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'language'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $article = Vwarticle::where('language',$request->language)->get();
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $article
            ];
        }
        return response()->json($re);
    }

    public function articleDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'article_id'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $article = Vwarticle::find($request->article_id);
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $article
            ];
        }
        return response()->json($re);
    }
    public function page(Request $request, $slug)
    {
        // dd($slug);
        $validator = Validator::make($request->all(), [
            'language'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $page = Vwpage::where('slug',$slug)->where('language',$request->language)->first();
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $page
            ];
        }
        
        return response()->json($re);
    }

    public function otherApp(Request $request)
    {
        // dd($slug);
        
            $otherapps = Vwotherapp::get();
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $otherapps
            ];
        
        
        return response()->json($re);
    }

    public function Renew(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'language'          => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
        
            $renew = Vwrenew::where('language',$request->language)->get();
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $renew
            ];
        }
        
        return response()->json($re);
    }

    public function add_feedback(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'          => 'required',
            'description'      => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input      = $request->all();
            $feedback   = new Vwfeedback($input);
            $feedback->save();
            
            // dd($poster);
            // foreach($poster as $poster)
            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => $feedback
            ];
        }
        
        return response()->json($re);
    }


}
