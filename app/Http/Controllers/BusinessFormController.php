<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Type;
use App\Models\BusinessForm;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;

class BusinessFormController extends Controller
{
    public function index(Request $request)
    {  
        if($request->ajax()) : 
            $data = BusinessForm::latest()->get();
            return DataTables::of($data)
                ->addIndexColumn()
                ->addColumn('customer_name', function($row){
                    $html =  '<div>
                        '.$row->title.' '.$row->full_name.'
                    </div>';

                    return $html;
                })
                ->addColumn('payoutIn', function($row){
                    $html =  '<div>
                        '.$row->payout_in.' '.$row->payout_out.'
                    </div>';

                    return $html;
                })
                ->addColumn('last_update', function($row){
                    $html =  '<div>
                        '.date('d.m.Y h:i A', strtotime($row->updated_at)).'
                    </div>';

                    return $html;
                })
                ->addColumn('created_date', function($row){
                    $html =  '<div>
                        '.date('d.m.Y h:i A', strtotime($row->created_at)).'
                    </div>';

                    return $html;
                })
                ->addColumn('policy_pdf', function($row){
                    if(!empty($row->policy_pdf_url))
                        $html =  '<div>
                        <a href="'.$row->policy_pdf_url.'" target="_blank">View Pdf</a>
                        </div>';
                    else $html = "Not uploaded.";

                    return $html;
                })
                ->addColumn('show', function($row){
                    $btn = '<a type="button" style="width: 100px; white-space: nowrap; height: auto;" class="btn btn-primary btn-lg" href="'.route('admin.business_form.show', $row->id).'"> View </a>';

                    return $btn;
                })
                ->rawColumns(['customer_name','payoutIn','last_update','created_date','policy_pdf','show'])
                ->make(true);
        endif;

        $page = 'business_form.list';
        $data = compact('page');
        return view('backend.layout.master', $data);

        
    }

    public function show( BusinessForm $business_form)
    {
        $page = 'business_form.show';
        $data = compact('page','business_form');

        return view('backend.layout.master',$data);
    }
}
