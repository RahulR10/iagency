<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;

use App\Models\WebCategories;
use App\Models\WebBlog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        $categories = WebCategories::pluck('title', 'slug');

        $blogs = WebBlog::latest()->get();

        return view('frontend.pages.blog.index', compact('categories', 'blogs'));
    }

    public function show(WebBlog $blog)
    {
        // dd($blog);
        return view('frontend.pages.blog.show',compact('blog'));
    }
}
