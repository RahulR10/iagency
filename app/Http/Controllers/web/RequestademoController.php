<?php

namespace App\Http\Controllers\web;

use App\Models\WebRequestademo;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RequestademoController extends Controller
{
    public function store(Request $request)
    {
        $webRequestademo = new WebRequestademo();
        $webRequestademo->name = $request->name;
        $webRequestademo->email = $request->email;
        $webRequestademo->mobile = $request->mobile;
        $webRequestademo->description = $request->description;
        $webRequestademo->web_product_id =$request->web_product_id;

        $webRequestademo->save();

        return redirect()->back()->with('success','Success! Record has Been added');
    }
}
