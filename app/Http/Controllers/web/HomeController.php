<?php

namespace App\Http\Controllers\web;
use App\Models\WebSlider;
use App\Models\WebTestimonial;
use App\Models\WebProduct;
use App\Http\Controllers\Controller;


use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $all_products = WebProduct::pluck('title', 'id');
        $sliders = WebSlider::latest()->get();
        $testimonials = WebTestimonial::latest()->get(); 
        $products = WebProduct::with('web_pricing')->latest()->get(); 
        return view('frontend.pages.homepage',compact('sliders', 'testimonials','all_products', 'products'));
    }
}
