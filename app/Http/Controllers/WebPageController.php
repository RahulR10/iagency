<?php

namespace App\Http\Controllers;

use App\Models\WebPage;
use Illuminate\Http\Request;

class WebPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = WebPage::paginate(10);
        return view('backend.inc.wb_pages.list',['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request-> flush();
        return view('backend.inc.wb_pages.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WebPage $page)
    {
        $page->title = $request->title;
        $page ->excerpt = $request->excerpt;
        
        $page ->save();

        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/page','public');
            $page->image = $path;
            $page->save();
        }

        return redirect(route('webadmin.page.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebPage  $webPage
     * @return \Illuminate\Http\Response
     */
    public function show(WebPage $webPage)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebPage  $webPage
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, WebPage $page)
    {
        // dd($page);
        $request->replace($page->toArray());
        $request->flash();

        $data= compact('page');
        return view('backend.inc.wb_pages.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebPage  $webPage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebPage $page)
    {
        $page->title = $request->title;
        $page ->excerpt = $request->excerpt;
        $page ->save();

        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/page','public');
            $page->image = $path;
            $page->save();
        }

        return redirect(route('webadmin.page.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebPage  $webPage
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebPage $page)
    {
        $page->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
