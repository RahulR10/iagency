<?php

namespace App\Http\Controllers;

use App\Http\Requests\VariantRequest;
use Illuminate\Http\Request;
use App\Models\Make;
use App\Models\Vwvariant;
use App\Models\VwfuelType;
use App\Models\Vwmodel;
use App\Models\Type;

class VwvariantController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vwvariants = Vwvariant::latest()->paginate(10);

        $page  = 'variant.list';
        $data  = compact('page','vwvariants');
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $type  = Type::pluck('name','id');
        $make = [];
        $vwmodel = Vwmodel::pluck('name','id');
        $vw_fuel_type = VwfuelType::pluck('name','id');
        $page   = 'variant.add';
        $title  = 'Add Variant';
        $data   = compact('page','title','vwmodel','vw_fuel_type','make','type');

        // dd($data);

        return view('backend.layout.master',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(VariantRequest $request)
    {
        $vwvariant = new Vwvariant;

        $vwvariant->name    = $request->name;
        $vwvariant->slug    = \Str::slug($request->name, "-");
        $vwvariant->cubic_capacity    = $request->cubic_capacity;
        $vwvariant->seating_capacity    = $request->seating_capacity;
        $vwvariant->vwmodel_id    = $request->vwmodel_id;
        $vwvariant->vwmfuel_type_id    = $request->vwmfuel_type_id;
        
        $vwvariant->save();
        $vwvariant->slug .= '-'.$vwvariant->id;
        $vwvariant->save();

        return redirect(route('admin.variant.index'))->with('success', 'Success! Record has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vwvariant $variant)
    {
        $request_params = $variant->toArray();
        $request_params['make_id'] = $variant->model->make_id;
        $request_params['type_id'] = !empty($variant->model->make_id) ? $variant->model->make->type_id : null;
        // dd($variant);
        $request->replace($request_params);
        $request->flash();

        $type  = Type::pluck('name','id');
        $make = [];
        if($request_params['type_id']) {
            $make = Make::where('type_id', $request_params['type_id'])->pluck('name', 'id');
        }
        $vwmodel = Vwmodel::pluck('name','id');
        $vw_fuel_type = VwfuelType::pluck('name','id');

        $page = 'variant.edit';
        $data= compact('variant','page', 'type', 'make', 'vwmodel', 'vw_fuel_type');
        return view('backend.layout.master',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Vwvariant $vwvariant)
    {
        $vwvariant->name    = $request->name;
        // $vwvariant->slug    = $request->slug;
        $vwvariant->cubic_capacity    = $request->cubic_capacity;
        $vwvariant->seating_capacity    = $request->seating_capacity;
        $vwvariant->vwmodel_id    = $request->vwmodel_id;
        $vwvariant->vwmfuel_type_id    = $request->vwmfuel_type_id;
        
        $vwvariant->save();
        // $vwvariant->slug .= '-'.$vwvariant->id;
        // $vwvariant->save();

        return redirect(route('admin.variant.index'))->with('success', 'Success! Record has been added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwvariant $vwvariant)
    {
        // dd($vwvariant);
        $vwvariant->delete();

        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
