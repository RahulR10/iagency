<?php

namespace App\Http\Controllers;

// use App\Models\Type;

use App\Http\Requests\ModelRequest;
use App\Models\Vwmodel;
use App\Models\Make;
use App\Models\Type;
use Dotenv\Validator;
use Illuminate\Http\Request;

class VModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vmodels = Vwmodel::latest()->paginate(10);

        $page  = 'model.list';
        $data  = compact('page','vmodels');
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        // $this->validate($request,[
        //     'name' => 'unique',
        // ]);

        $type  = Type::pluck('name','id');
        $make  = [];
        $page  = 'model.add';
        $title = 'Add Model';
        $data = compact('page','title','make','type');

        return view('backend.layout.master',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ModelRequest $request)
    {
        $vmodel = new Vwmodel;
        
        $vmodel->name = $request->name; 
        $vmodel->slug = \Str::slug($request->name, "-"); 
        $vmodel->make_id = $request->make_id; 
        $vmodel->save();
        $vmodel->slug .= '-'.$vmodel->id;
        $vmodel->save();
        
        return redirect(route('admin.vmodel.index'))->with('success', 'Success! Record has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vwmodel $vmodel)
    {
        $request->replace($vmodel->toArray());
        $request->flash();

        $type  = Type::pluck('name','id');
        $make  = [];

        $page = 'model.edit';
        $data = compact('vmodel','type','make','page');

        return view('backend.layout.master',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Vwmodel $vmodel )
    {
        $vmodel->name = $request->name; 
        $vmodel->slug = $request->slug; 
        $vmodel->make_id = $request->make_id; 
        $vmodel->save();
        
        return redirect(route('admin.vmodel.index'))->with('success', 'Success! Record has been added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwmodel $vmodel)
    {
        $vmodel->delete();

        return redirect(route('admin.vmodel.index'))->with('success','Your data has been deleted');
    }
}
