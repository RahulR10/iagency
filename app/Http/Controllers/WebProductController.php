<?php

namespace App\Http\Controllers;

use App\Models\WebProduct;
use Illuminate\Http\Request;

class WebProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = WebProduct::paginate(10);
        return view('backend.inc.wb_product.list',['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request-> flush();
        return view('backend.inc.wb_product.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WebProduct $product)
    {   
        $product->title = $request->title;
        $product->url  = $request->url;
        $product ->excerpt = $request->excerpt;
        $product ->save();

        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/product','public');
            $product->image = $path;
            $product->save();
        }

        return redirect(route('webadmin.product.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebProduct  $webProduct
     * @return \Illuminate\Http\Response
     */
    public function show(WebProduct $webProduct)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebProduct  $webProduct
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, WebProduct $product)
    {
        $request->replace($product->toArray());
        $request->flash();

        $data= compact('product');
        return view('backend.inc.wb_product.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebProduct  $webProduct
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebProduct $product)
    {
        $product->title = $request->title;
        $product->url  = $request->url;
        $product ->excerpt = $request->excerpt;
        $product ->save();

        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/product','public');
            $product->image = $path;
            $product->save();
        }

        return redirect(route('webadmin.product.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebProduct  $webProduct
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebProduct $product)
    {
        $product->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
