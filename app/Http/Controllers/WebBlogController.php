<?php

namespace App\Http\Controllers;

use App\Models\WebCategories;
use App\Models\WebBlog;
use Illuminate\Http\Request;

class WebBlogController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs =WebBlog::latest()->paginate(10);
        return view('backend.inc.wb_blog.list',['blog' => $blogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $categories = WebCategories::pluck('title','id');
        $request->flush();
        return view('backend.inc.wb_blog.add',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WebBlog $blog)
    {
        $blog->title = $request->title;
        $blog->web_category_id = $request->web_category_id;
        $blog->excerpt =$request->excerpt;
        $blog->save();

        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/blog','public');
            $blog->image = $path;
            $blog->save();
        }

        return redirect(route('webadmin.blog.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebBlog  $webBlog
     * @return \Illuminate\Http\Response
     */
    public function show(WebBlog $webBlog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebBlog  $webBlog
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, WebBlog $blog )
    {
        $categories = WebCategories::pluck('title','id');
        $request->replace($blog-> toArray());
        $request->flash();

        $data = compact('blog','categories');
        return view('backend.inc.wb_blog.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebBlog  $webBlog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebBlog $blog)
    {
        $blog->title = $request->title;
        $blog->web_category_id = $request->web_category_id;
        $blog->image =$request->image;
        $blog->excerpt =$request->short_description;
        $blog->save();

        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/blog','public');
            $blog->image = $path;
            $blog->save();
        }

        return redirect(route('webadmin.blog.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebBlog  $webBlog
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebBlog $blog)
    {
        $blog->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
