<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\VwfuelType;
use App\Http\Requests\FuelTypeRequest;


class FuelTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fuels = VwfuelType::latest()->paginate(10);

        $page = 'fuel.list';
        $data = compact('fuels','page');
        return view('backend.layout.master',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page ='fuel.add';
        $title = 'Add Fuel';
        $data = compact('page','title');

        return view('backend.layout.master',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FuelTypeRequest $request)
    {
        $fuel = new VwfuelType;
        $fuel->name = $request->name; 
        $fuel->slug = \Str::slug($request->name,"-"); 

        $fuel->save();
        $fuel->slug .= '-'.$fuel->id;
        $fuel->save();

        return redirect(route('admin.fuel.index'))->with('success', 'Success! Record has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, VwfuelType $fuel)
    {
        $request->replace($fuel->toArray());
        $request->flash();
        $page = 'fuel.edit';
        $data = compact('fuel','page');
        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,VwfuelType $fuel)
    {
        $fuel->name = $request->name; 
        $fuel->slug = $request->slug; 

        $fuel->save();

        return redirect(route('admin.fuel.index'))->with('success', 'Success! Record has been added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(VwfuelType $fuel)
    {
        $fuel->delete();
        
        return redirect()->back()->with('success','Your data has been Deleted.');
    }
}
