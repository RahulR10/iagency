<?php

namespace App\Http\Controllers;

use App\Models\WebPricing;
use App\Models\WebProduct;
use Illuminate\Http\Request;

class WebPricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $prices = WebPricing::paginate(10);
        return view('backend.inc.wb_pricing.list',['prices' => $prices]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $products = WebProduct::pluck('title', 'id');
        $request-> flush();
        return view('backend.inc.wb_pricing.add',compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WebPricing $price)
    {
        $price->title = $request->title;
        $price->price = $request->price;
        $price->description = $request->description;
        $price->save();
        
        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/price','public');
            $price->image = $path;
            $price->save();
        }

        return redirect(route('webadmin.price.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebPricing  $webPricing
     * @return \Illuminate\Http\Response
     */
    public function show(WebPricing $webPricing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebPricing  $webPricing
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,WebPricing $price)
    {

        $products = WebProduct::pluck('title','id');

        $request->replace($price->toArray());
        $request->flash();

        $data= compact('price','products');
        return view('backend.inc.wb_pricing.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebPricing  $webPricing
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebPricing $price)
    {
        $price->title = $request->title;
        
        $price->price = $request->price;
        $price->description = $request->description;
        $price->save();
        
        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/price','public');
            $price->image = $path;
            $price->save();
        }

        return redirect(route('webadmin.price.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebPricing  $webPricing
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebPricing $price)
    {
        $price->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
