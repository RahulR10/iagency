<?php

namespace App\Http\Controllers;

use App\Models\WebTestimonial;
use Illuminate\Http\Request;

class WebTestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $testimonials = WebTestimonial::paginate(10);
        return view('backend.inc.wb_testimonial.list',['testimonials' => $testimonials]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request-> flash();
        return view('backend.inc.wb_testimonial.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WebTestimonial $testimonial)
    {
        $testimonial->title =$request->title;
        $testimonial->excerpt =$request->excerpt;
        $testimonial->save();

        return redirect(route('webadmin.testimonial.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebTestimonial  $webTestimonial
     * @return \Illuminate\Http\Response
     */
    public function show(WebTestimonial $webTestimonial)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebTestimonial  $webTestimonial
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,WebTestimonial $testimonial)
    {
        $request->replace($testimonial->toArray());
        $request->flash();

        $data= compact('testimonial');
        return view('backend.inc.wb_testimonial.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebTestimonial  $webTestimonial
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebTestimonial $testimonial)
    {
        $testimonial->title =$request->title;
        $testimonial->excerpt =$request->excerpt;
        $testimonial->save();

        return redirect(route('webadmin.testimonial.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebTestimonial  $webTestimonial
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebTestimonial $testimonial)
    {
        $testimonial->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
