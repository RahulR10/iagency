<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwrenew;
use Illuminate\Http\Request;

class RenewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('ab');
        $lists = Vwrenew::latest()->get();

        $page  = 'vw_renew.list';
        $title = 'Renew Insurance list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page  = 'vw_renew.add';
        $title = 'Add Renew Insurance';
        $data  = compact('page', 'title');

        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Vwrenew;
        $input            = $request->record;        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/renew/', $filename);
                $record->image = 'renew/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_renew.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.vw_renew.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwrenew  $vwrenew
     * @return \Illuminate\Http\Response
     */
    public function show(Vwrenew $vwrenew)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwrenew  $vwrenew
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vwrenew $vw_renew)
    {
        $editData =  ['record' => $vw_renew->toArray()];
        $request->replace($editData);
        $request->flash();

        // set page and title ------------------
        $page = 'vw_renew.edit';
        $title = 'Edit Renew Insurance';
        $data = compact('page', 'title', 'vw_renew');

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwrenew  $vwrenew
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwrenew $vw_renew)
    {
        $record     = $vw_renew;
        $input      = $request->record;

        
        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/renew/', $filename);
                $record->image = 'renew/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_renew.index'))->with('success', 'Success! Record has been edited');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwrenew  $vwrenew
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwrenew $vw_renew)
    {
        $vw_renew->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
