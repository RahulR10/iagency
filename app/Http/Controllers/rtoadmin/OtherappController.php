<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwotherapp;
use Illuminate\Http\Request;

class OtherappController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('ab');
        $lists = Vwotherapp::latest()->get();

        $page  = 'vw_otherapp.list';
        $title = 'Other App list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page  = 'vw_otherapp.add';
        $title = 'Add Other App';
        $data  = compact('page', 'title');

        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Vwotherapp;
        $input            = $request->record;        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/otherapp/', $filename);
                $record->image = 'otherapp/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_otherapp.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.vw_otherapp.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwotherapp  $vwotherapp
     * @return \Illuminate\Http\Response
     */
    public function show(Vwotherapp $vwotherapp)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwotherapp  $vwotherapp
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vwotherapp $vw_otherapp)
    {
        $editData =  ['record' => $vw_otherapp->toArray()];
        $request->replace($editData);
        $request->flash();

        // set page and title ------------------
        $page = 'vw_otherapp.edit';
        $title = 'Edit Other App';
        $data = compact('page', 'title', 'vw_otherapp');

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwotherapp  $vwotherapp
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwotherapp $vw_otherapp)
    {
        $record     = $vw_otherapp;
        $input      = $request->record;

        
        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/otherapp/', $filename);
                $record->image = 'otherapp/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_otherapp.index'))->with('success', 'Success! Record has been edited');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwotherapp  $vwotherapp
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwotherapp $vw_otherapp)
    {
        $vw_otherapp->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
