<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwpage;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('ab');
        $lists = Vwpage::latest()->get();

        $page  = 'vw_page.list';
        $title = 'Page list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page  = 'vw_page.add';
        $title = 'Add Page';
        $data  = compact('page', 'title');

        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Vwpage;
        $input            = $request->record;        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/page/', $filename);
                $record->image = 'page/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_page.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.vw_page.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwpage  $vwpage
     * @return \Illuminate\Http\Response
     */
    public function show(Vwpage $vwpage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwpage  $vwpage
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vwpage $vw_page)
    {
        $editData =  ['record' => $vw_page->toArray()];
        $request->replace($editData);
        $request->flash();

        // set page and title ------------------
        $page = 'vw_page.edit';
        $title = 'Edit Page';
        $data = compact('page', 'title', 'vw_page');

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwpage  $vwpage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwpage $vw_page)
    {
        $record     = $vw_page;
        $input      = $request->record;

        
        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/page/', $filename);
                $record->image = 'page/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_page.index'))->with('success', 'Success! Record has been edited');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwpage  $vwpage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwpage $vw_page)
    {
        $vw_page->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
