<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwvehicle;
use Illuminate\Http\Request;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $lists = Vwvehicle::latest()->get();

        $page  = 'vw_vehicle.list';
        $title = 'Vehicle list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwvehicle  $vwvehicle
     * @return \Illuminate\Http\Response
     */
    public function show(Vwvehicle $vwvehicle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwvehicle  $vwvehicle
     * @return \Illuminate\Http\Response
     */
    public function edit(Vwvehicle $vwvehicle)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwvehicle  $vwvehicle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwvehicle $vwvehicle)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwvehicle  $vwvehicle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwvehicle $vwvehicle)
    {
        //
    }

    public function detail(Request $request, Vwvehicle $id)
    {
        
        $list = Vwvehicle::with('user','vehicle_documents')->find($id->id);
        
        $page  = 'vw_vehicle.detail';
        $title = 'Vehicle Info';
        $data  = compact('page', 'title', 'list');

        return view('backend.layout.master', $data);
    }
}
