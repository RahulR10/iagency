<?php

namespace App\Http\Controllers\rtoadmin;

use App\Http\Controllers\Controller;
use App\Models\Vwarticle;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // dd('ab');
        $lists = Vwarticle::latest()->get();

        $page  = 'vw_article.list';
        $title = 'Article list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $page  = 'vw_article.add';
        $title = 'Add Article';
        $data  = compact('page', 'title');

        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.name'   => 'required|string'
        ];

        $messages = [
            'record.name'  => 'Please Enter Name.',
            'image'  => 'Please Select Image'
        ];

        $request->validate($rules, $messages);

        $record           = new Vwarticle;
        $input            = $request->record;        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/article/', $filename);
                $record->image = 'article/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_article.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.vw_article.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwarticle  $vwarticle
     * @return \Illuminate\Http\Response
     */
    public function show(Vwarticle $vwarticle)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vwarticle  $vwarticle
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Vwarticle $vw_article)
    {
        $editData =  ['record' => $vw_article->toArray()];
        $request->replace($editData);
        $request->flash();

        // set page and title ------------------
        $page = 'vw_article.edit';
        $title = 'Edit Article';
        $data = compact('page', 'title', 'vw_article');

        return view('backend.layout.master', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwarticle  $vwarticle
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwarticle $vw_article)
    {
        $record     = $vw_article;
        $input      = $request->record;

        
        

        $record->fill($input);
        if ($record->save()) {
            if ($request->hasFile('image')) {
                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $record->id) . '.' . $image->getClientOriginalExtension();
    
                $image->storeAs('public/article/', $filename);
                $record->image = 'article/' . $filename;
    
                $record->image .= '?v=' . time();
                $record->save();
            }
            return redirect(route('admin.vw_article.index'))->with('success', 'Success! Record has been edited');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwarticle  $vwarticle
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwarticle $vw_article)
    {
        $vw_article->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
