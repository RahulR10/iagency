<?php

namespace App\Http\Controllers;

use App\Models\WebSlider;
use Illuminate\Http\Request;

class WebSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = WebSlider::paginate(10);
        return view('backend.inc.wb_slider.list',['slider' => $sliders]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $request-> flush();
        return view('backend.inc.wb_slider.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WebSlider $slider)
    {
        $slider->title = $request->title;
        $slider->excerpt = $request->excerpt;
        $slider->save();

        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/slider','public');
            $slider->image = $path;
            $slider->save();
        }

        return redirect(route('webadmin.slider.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebSlider  $webSlider
     * @return \Illuminate\Http\Response
     */
    public function show(WebSlider $webSlider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebSlider  $webSlider
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request ,WebSlider $slider)
    {
        $request->replace($slider->toArray());
        $request->flash();

        $data= compact('slider');
        return view('backend.inc.wb_slider.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebSlider  $webSlider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebSlider $slider)
    {
        $slider->title = $request->title;
        $slider->excerpt = $request->excerpt;
        $slider->save();

        if($request->hasFile('image'))
        {
            $path = $request->file('image')->store('/website/slider','public');
            $slider->image = $path;
            $slider->save();
        }

        return redirect(route('webadmin.slider.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebSlider  $webSlider
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebSlider $slider)
    {
        $slider->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
