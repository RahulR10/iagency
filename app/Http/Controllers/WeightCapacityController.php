<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\WeightCapacity;
use App\Models\Type;

class WeightCapacityController extends Controller
{
    public function index()
    {
        $lists = WeightCapacity::paginate(10);

        // set page and title -------------
        $page  = 'weightcapacity.list';
        $title = 'weightcapacity list';
        $data  = compact('page', 'title', 'lists');

        return view('backend.layout.master', $data);
    }

    public function create()
    {
        $category = Type::get();
        $categoryArr  = ['' => 'Select Category'];
        if (!$category->isEmpty()) {
            foreach ($category as $pcat) {
                $categoryArr[$pcat->id] = $pcat->name;
            }
        }

        $page  = 'weightcapacity.add';
        $title = 'Add weightcapacity';
        $data  = compact('page', 'title', 'categoryArr');

        return view('backend.layout.master', $data);
    }

    public function store(Request $request)
    {
        $rules = [
            'record'        => 'required|array',
            'record.weight_range'   => 'required|string',
            'record.type_id'   => 'required'
        ];

        $message = [
            'record.weight_range'  => 'Please Enter weight_range.',
        ];

        $request->validate($rules, $message);

        $record           = new WeightCapacity;
        $input            = $request->record;

        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.weightcapacity.index'))->with('success', 'Success! New record has been added.');
        } else {
            return redirect(route('admin.weightcapacity.index'))->with('danger', 'Error! Something going wrong.');
        }
    }

    public function edit(Request $request, WeightCapacity $weightcapacity)
    {
        $category = Type::get();
        $categoryArr  = ['' => 'Select Category'];
        if (!$category->isEmpty()) {
            foreach ($category as $pcat) {
                $categoryArr[$pcat->id] = $pcat->name;
            }
        }

        $editData =  ['record' => $weightcapacity->toArray()];
        $request->replace($editData);
        $request->flash();

        $page  = 'weightcapacity.edit';
        $title = 'Edit weightcapacity';
        $data  = compact('page', 'title', 'weightcapacity', 'categoryArr');

        return view('backend.layout.master', $data);
    }

    public function update(Request $request, WeightCapacity $weightcapacity)
    {
        $record     = $weightcapacity;
        $input      = $request->record;

        $record->fill($input);
        if ($record->save()) {
            return redirect(route('admin.weightcapacity.index'))->with('success', 'Success! Record has been edided');
        }
    }

    public function destroy(WeightCapacity $weightcapacity)
    {
        $weightcapacity->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
