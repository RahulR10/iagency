<?php

namespace App\Http\Controllers;

use App\Models\WebContact;
use Illuminate\Http\Request;

class WebContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $contacts =WebContact::paginate(10);
        return view('backend.inc.wb_contact.list',['contact' => $contacts]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, WebContact $contact)
    {
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->mobile = $request->mobile;
        $contact->excerpt = $request->excerpt;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\WebContact  $webContact
     * @return \Illuminate\Http\Response
     */
    public function show(WebContact $webContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\WebContact  $webContact
     * @return \Illuminate\Http\Response
     */
    public function edit(WebContact $webContact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\WebContact  $webContact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, WebContact $webContact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\WebContact  $webContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(WebContact $contact)
    {
        $contact->delete();
        return redirect()->back()->with('success', 'Success! Record has been deleted');
    }
}
