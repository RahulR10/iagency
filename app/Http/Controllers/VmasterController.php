<?php

namespace App\Http\Controllers;

use App\Models\Vmaster;
use Illuminate\Http\Request;

class VmasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vmaster  $vmaster
     * @return \Illuminate\Http\Response
     */
    public function show(Vmaster $vmaster)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vmaster  $vmaster
     * @return \Illuminate\Http\Response
     */
    public function edit(Vmaster $vmaster)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vmaster  $vmaster
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vmaster $vmaster)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vmaster  $vmaster
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vmaster $vmaster)
    {
        //
    }
}
