<?php

namespace App\Http\Controllers;

use App\Http\Requests\MakeTypeRequest;
use Illuminate\Http\Request;
use App\Models\Make;
use App\Models\Type;

class MakeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $makes = Make::latest()->paginate(10);

        $page  = 'make.list';
        $data  = compact('page','makes');
        return view('backend.layout.master', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $type  = Type::pluck('name','id');
        $page  = 'make.add';
        $title = 'Add HealthAge';
        $data  = compact('page', 'title','type');

        return view('backend.layout.master', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MakeTypeRequest $request)
    {
        $make = new Make;
        $make->type_id = $request->type_id;
        $make->name = $request->name;
        $make->save();

        return redirect(route('admin.make.index'))->with('success', 'Success! Record has been added');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, Make $make)
    {
        $request->replace($make->toArray());
        $request->flash();

        $type  = Type::pluck('name','id');
        $page  = 'make.edit'; 
        $data  = compact('make','type','page');

        return view('backend.layout.master', $data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,Make $make)
    {
        $make->type_id = $request->type_id;
        $make->name = $request->name;
        $make->save();

        return redirect(route('admin.make.index'))->with('success', 'Success! Record has been added');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Make $make)
    {
        $make->delete();

        return redirect(route('admin.make.index'))->with('success','Your data has been deleted');
    }
}
