<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Auth;
// use Hash;
use App\Models\User;
use App\Models\SubscriptionPlan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
// use Excel;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    public function index()
    {
        $data = User::with('subscription_plan')->latest()->get();
        return DataTables::of($data)
        ->addIndexColumn()
        // ->addColumn('contact_info', function ($row) {
        //     $html = '<div>
        //         <div>
        //             <strong>'.$row->name.'</strong>
        //         </div>
        //         <div class="row">
        //             <div class="col-4">
        //                 <strong>Username:</strong>
        //             </div>
        //             <div class="col-8">
        //                 '.$row->user_name.'
        //             </div>
        //         </div>
        //         <div>
        //             <strong>Address:<br></strong> '.$row->full_address.'
        //         </div>
        //     </div>';

        //     return $html;
        // })
        ->addColumn('action', function($row){       
            $btn = '<button 
                type="button" 
                style="width: 100px; white-space: nowrap; height: auto;" 
                class="btn btn-primary btn-lg toggle-user-edit-modal" 
                data-url="'.route('admin.users-update', $row->id).'"
                data-valid="'.$row->valid_upto.'"
                data-plan="'.$row->subscription_plan_id.'"
            >';
            if($row->subscription_plan_id)
                $btn .=    @$row->subscription_plan->name.'<br>'.date("d.m.Y",strtotime($row->valid_upto));
            else
                $btn .= "Select Plan";
            $btn .= '</button>';
            return $btn;
        })
        ->addColumn('date', function($row){
            $html = '<div>
                <div class="row g-0">
                    <div class="col-12">
                        '.date('d.m.Y h:i A', strtotime($row->created_at)).'
                    </div>
                </div>
            </div>';
            return $html;
        })
        ->addColumn('mobile', function($row){
            $className = $row->otp_verified == 'true' ? 'text-success' : 'text-danger';
            
            $html = '<div>
                <div class="col-12 '.$className.'">
                    '.$row->mobile.'
                </div>
            </div>';
            return $html;
        })
        ->addColumn('email', function($row) {
            $classEmail = $row->email_verified == 'true' ? 'text-success' : 'text-danger';

            $html = '<div>
                <div class="col-12 '.$classEmail.'">
                    '.$row->email.'
                </div>
            </div>';
            return $html;
        })
        ->rawColumns(['action', 'date','mobile','email'])
        ->make(true);
            
    }
    
    public function profile(Request $request)
    {
        $lists = Auth::guard()->user();

        $editData = $lists->toArray();
        $request->replace($editData);
        $request->flash();
        // dd($editData);
        // set page and title ------------------
        $page  = 'user.list';
        $title = 'User list';
        $data  = compact('page', 'title', 'lists');

        // return data to view
        return view('backend.layout.master', $data);
    }

    public function edit_profile(Request $request)
    {
        $rules = [
            'current_password' => 'required|string',
            'new_password'     => 'required|string|min:6'
        ];
        $request->validate($rules);

        $new_password = Hash::make($request->new_password);

        if ($request->current_password != $request->new_password) {
            $record             = Auth::guard()->user();
            $record->name       = $request->name;
            $record->email      = $request->email;
            $record->password   = $new_password;
            // dd($record);
            $record->save();
            return redirect()->back()->with('success', 'Your password has been changed successfully.');
        } else {
            return redirect()->back()->with('danger', 'Error!! Current and new password are same.');
        }
    }

    public function list(Request $request)
    {
        if ($request->limit) {
            $limit = $request->limit;
        } else {
            $limit = 20;
        }
        
        $query = User::where('role_id', 2);
        // if($request->s){
        // dd($query);
        $query->where('name', 'LIKE', "%{$request->s}%");
        // $query->where('user_name', 'LIKE', "%{$request->s}%")where('email', 'LIKE', "%{$request->s}%")->where('mobile', 'LIKE', "%{$request->s}%");
        // $query->whereHas('address', function($q) use($request){
        //     $q->where('City', 'LIKE', "%{$request->s}%");
        //     $q->where('State', 'LIKE', "%{$request->s}%");
        //     $q->where('Pincode', 'LIKE', "%{$request->s}%");
        // });
        // }
        $lists = $query->orderBy('id', 'desc')->get();
        foreach ($lists as $list) {
            if ($list->accept_code) {
                $list->referal_user = User::where('referal_code', $list->accept_code)->first();
            }
        }

        $subscription_plan = SubscriptionPlan::pluck('name', 'id');

        // set page and title ------------------
        $page  = 'user.userlist';
        $title = 'User list';
        $data  = compact('page', 'title', 'lists', 'subscription_plan');
        // dd($lists);
        // return data to view
        return view('backend.layout.master', $data);
    }

    public function update(Request $request, User $user)
    {
        $user->valid_upto = $request->valid_upto;
        $user->subscription_plan_id = $request->subscription_plan_id;
        $user->save();

        return redirect(route('admin.users-list'))->with('success','Success ! Your Record has been Updated ');
    }

    public function export(Request $request)
    {
        $exl = Excel::download(new User, 'users.xlsx');
        return $exl;
    }
}
