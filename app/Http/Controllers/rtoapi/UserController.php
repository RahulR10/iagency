<?php

namespace App\Http\Controllers\rtoapi;

use App\Http\Controllers\Controller;
use Validator;
use App\Models\Vwuser;
use App\Models\Setting;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function sendOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'           => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $setting = Setting::find(1);
            $input = $request->all();
            $user = Vwuser::where('mobile', $input['mobile'])->first();

            if ($user) {
                $otp = rand(100000, 999999);
                // Send SMS
                // $msg    = urlencode("Your one time password in ".$setting->title." is ".$otp." ");
                $msg    = urlencode("Dear " . $setting->title . " user, " . $otp . " is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE.
Regards
IAGENCY");
                $apiUrl = str_replace(["[MESSAGE]", "[MOBILE]"], [$msg, request('mobile')], $setting->sms_api);
                $sms    = file_get_contents($apiUrl);
                $user->otp = $otp;
                $user->save();

                $re = [
                    'status'    => true,
                    'message'   => 'Success! Otp Send Successfully',
                    'data'      => $user
                ];
            } else {
                $otp = rand(100000, 999999);
                // Send SMS
                // $msg    = urlencode("Your one time password in ".$setting->title." is ".$otp." ");
                $msg    = urlencode("Dear " . $setting->title . " user, " . $otp . " is the OTP for your mobile number verification. PLS DO NOT SHARE WITH ANYONE.
Regards
IAGENCY");
                $apiUrl = str_replace(["[MESSAGE]", "[MOBILE]"], [$msg, request('mobile')], $setting->sms_api);
                $sms    = file_get_contents($apiUrl);
                $input['otp'] = $otp;
                $new_user = new Vwuser($input);
                $new_user->save();
                $re = [
                    'status'    => true,
                    'message'   => 'Success! Otp Send Successfully',
                    'data'      => $new_user
                ];
            }
        }
        return response()->json($re);
    }


    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required',
            'otp'      => 'required'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            // Check if mobile number exists or not

            $user = Vwuser::where('mobile', request('mobile'))->where('otp', request('otp'))->first();


            if (!empty($user->id)) {

                $input = [
                    'otp_verified'  => true,
                    'device_type'   => request('device_type'),
                    'device_id'     => request('device_id'),
                    'fcm_id'        => request('fcm_id')
                ];

                $user->fill($input)->save();
                // $token = $user->createToken('RTO-App')->accessToken;

                $re = [
                    'status'    => true,
                    'message'   => 'Success!! Login successfully.',
                    'data'      => $user,
                ];

            } else {
                $re = [
                    'status'    => false,
                    'message'   => 'Error!! Otp not matched.',
                ];
            }
        }
            
            // $curl = curl_init();

            // curl_setopt_array($curl, array(
            //     CURLOPT_URL => "https://api.zeptomail.in/v1.1/email/template",
            //     CURLOPT_RETURNTRANSFER => true,
            //     CURLOPT_ENCODING => "",
            //     CURLOPT_MAXREDIRS => 10,
            //     CURLOPT_TIMEOUT => 30,
            //     CURLOPT_SSLVERSION => CURL_SSLVERSION_TLSv1_2,
            //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            //     CURLOPT_CUSTOMREQUEST => "POST",
            //     CURLOPT_POSTFIELDS => '{
            //         "mail_template_key": "2518b.21db5ddb4469b46b.k1.7b95e680-3338-11ed-b899-525400b0b0f3.18335d254e8",
            // "bounce_address":"bounce@bounce.mailserver.iagency.in",
            // "from": { "address": "noreply@mailserver.iagency.in"},
            // "to": [{"email_address": {"address": "sudarshandubaga@gmail.com","name": "Sudarshan Dubaga"}}]
            // }
            // ]
            // }',
            //     CURLOPT_HTTPHEADER => array(
            //         "accept: application/json",
            //         "authorization: Zoho-enczapikey PHtE6r0FQOm+gmR5+0BUsffsEcL3MIwt+u1vKQcUuNsRDPBXTk1c+N0vmzO2okgqVKNAQf6SyophtL7Iuu7UIG7uPD1LCWqyqK3sx/VYSPOZsbq6x00btFkYfkzYVoTtcdJq1SHevNrSNA==",
            //         "cache-control: no-cache",
            //         "content-type: application/json",
            //     ),
            // ));

            // $response = curl_exec($curl);
            // $err = curl_error($curl);

            // curl_close($curl);

            // if ($err) {
            //     echo "cURL Error #:" . $err;
            // } else {
            //     echo $response;
            // }

        return response()->json($re);
    }

    public function editProfile(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'user_id'   => 'required',
            'first_name' => 'required|unique:vwusers,first_name,' . $request->user_id,
            'last_name' => 'required|unique:vwusers,last_name,' . $request->user_id,
            'email'     => 'required|unique:vwusers,email,' . $request->user_id,
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            // Check if mobile number exists or not
            $input = $request->all();

            $user = Vwuser::find($request->user_id);
            $user->first_name   = $request->first_name;
            $user->last_name    = $request->last_name;
            $user->email        = $request->email;

            $user->save();

            $re = [
                'status'    => true,
                'message'   => 'Success',
                'data'      => $user,
            ];
        }
        return response()->json($re);
    }

    public function logout(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile'     => 'required',
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $user = Vwuser::where('mobile', request('mobile'))->first();
            $user->otp_verified = 'false';
            $user->device_type  = NULL;
            $user->device_id    = '';
            $user->fcm_id       = '';

            $user->save();
            $re = [
                'status'    => true,
                'message'   => 'Success! You are logout successfully.',
            ];
        }

        return response()->json($re);
    }

    public function profile(Request $request, Vwuser $id)
    {
        $re = [
            'status'    => true,
            'message'   => 'Success',
            'data'      => $id
        ];
        return response()->json($re);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vwuser  $vwuser
     * @return \Illuminate\Http\Response
     */
    public function show(Vwuser $vwuser)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vwuser  $vwuser
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vwuser $vwuser)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vwuser  $vwuser
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vwuser $vwuser)
    {
        //
    }
}
