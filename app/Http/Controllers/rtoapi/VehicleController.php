<?php

namespace App\Http\Controllers\rtoapi;

use App\Http\Controllers\Controller;
use Validator;
use App\Models\Vwuser;
use App\Models\Vwcategory;
use App\Models\Vwdocument;
use App\Models\Vwvehicledocument;
use App\Models\Vwvehicle;
use App\Models\Vwslider;
use Illuminate\Http\Request;
use DateTime;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'language'          => 'required',
            'user_id'           => 'required|numeric'
        ]);
        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            // Category 
            $query = Vwcategory::withCount(['vehicle' => function ($q) use ($request) {
                if ($request->reg_number != '') {

                    $q->where('reg_number', 'LIKE', '%' . $request->reg_number . '%');
                }
                $q->where('user_id', $request->user_id);
            }])->with(['vehicle' => function ($q) use ($request) {
                if ($request->reg_number != '') {

                    $q->where('reg_number', 'LIKE', '%' . $request->reg_number . '%');
                }
                $q->where('user_id', $request->user_id);
            }])->whereHas('vehicle', function ($q) use ($request) {
                if ($request->reg_number != '') {

                    $q->where('reg_number', 'LIKE', '%' . $request->reg_number . '%');
                }
                $q->where('user_id', $request->user_id);
            });

            $categories = $query->get();

            $license = Vwdocument::with(['document' => function ($q) use ($request) {
                $q->where('user_id', $request->user_id);
            }])->find(6);
            $lists = [];
            foreach ($categories as $cat) {
                if ($cat->vehicle_count != 0) {
                    $lists[] = $cat;
                }
            }

            $re = [
                'status'    => true,
                'message'   => 'Success!',
                'data'      => [
                    'category' => $lists,
                    'document' => $license
                ]
            ];
        }
        return response()->json($re);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'reg_number'    => 'required',
            'category_id'   => 'required',
            'user_id'       => 'required',
        ]);


        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->all();
            $vehicle = new Vwvehicle();
            $vehicle->reg_number    = $request->reg_number;
            $vehicle->category_id   = $request->category_id;
            $vehicle->user_id       = $request->user_id;
            $vehicle->save();

            $documents = $request->document;
            foreach ($documents as $d) {

                $d['vehicle_id'] = $vehicle->id;
                $document = new Vwvehicledocument($d);
                $document->save();
                if ($d['image']) {

                    $image = $d['image'];
                    $filename = 'IMAGE_' . sprintf('%06d', $document->id) . '.' . $image->getClientOriginalExtension();

                    $image->storeAs('public/document/', $filename);
                    $document->image = 'document/' . $filename;

                    $document->image .= '?v=' . time();
                    $document->save();
                }
            }
            
            $re = [
                'status'    => true,
                'message'   => 'New record add',
            ];
        }
        return response()->json($re);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    public function add_license(Request $request)
    {


        $validator = Validator::make($request->all(), [
            'user_id'       => 'required',
            'name'          => 'required'
        ]);


        if ($validator->fails()) {
            $re = [
                'status'    => false,
                'message'   => 'Validations errors found.',
                'errors'    => $validator->errors()
            ];
        } else {
            $input = $request->all();

            $vehicle = new Vwvehicledocument();
            $vehicle->user_id       = $request->user_id;
            $vehicle->name          = $request->name;
            $vehicle->document_id   = 6;
            $vehicle->expiry_date   = $request->expiry_date;

            $vehicle->save();
            if ($request->hasFile('image')) {
                // dd('sdkjdfj');

                // if (file_exists($item->logo)) {
                // unlink(storage_path('app/public/' . $item->image));
                // }

                $image = $request->image;
                $filename = 'IMAGE_' . sprintf('%06d', $vehicle->id) . '.' . $image->getClientOriginalExtension();

                $image->storeAs('public/document/', $filename);
                $vehicle->image = 'document/' . $filename;

                $vehicle->image .= '?v=' . time();
                $vehicle->save();
            }
            $re = [
                'status'    => true,
                'message'   => 'New record add',
            ];
        }
        return response()->json($re);
    }
    public function vehicle_detail(Request $request, $id)
    {

        $vehicle = Vwvehicle::with('user', 'category', 'vehicle_documents')->find($id);
        if ($vehicle) {
            $re = [
                'status'    => true,
                'message'   => 'Success',
                'data'      => $vehicle
            ];
        } else {
            $re = [
                'status'    => false,
                'message'   => 'Record not found',
            ];
        }
        return response()->json($re);
    }
}
