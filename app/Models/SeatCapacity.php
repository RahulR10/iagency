<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SeatCapacity extends Model
{
    protected $table = 'seatcapacities';
    protected $guarded = [];
    use HasFactory;

    public function category()
    {
        return $this->hasOne('App\Models\Type', 'id', 'type_id');
    }
}
