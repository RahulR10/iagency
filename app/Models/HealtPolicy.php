<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HealtPolicy extends Model
{
    use HasFactory;
    protected $with = ['insurer','broker','sub_agent'];

    public function insurer()
    {
        return $this->belongsTo(Company::class, 'isurer_id');
    }   

    public function broker()
    {
        return $this->belongsTo(Agent::class, 'broker_id');
    }

    public function sub_agent()
    {
        return $this->belongsTo(Agent::class, 'sub_agent_id');
    }
}
