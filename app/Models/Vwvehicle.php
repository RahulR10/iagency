<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vwvehicle extends Model
{
    use HasFactory;

    // protected $with = ['vehicle_documents'];

    public function vehicle_documents()
    {
        return $this->hasMany('App\Models\Vwvehicledocument', 'vehicle_id', 'id');
        // return $this->belongsTo(Vwvehicle::class);
    }
    public function user()
    {
        return $this->belongsTo(Vwuser::class);
    }
    public function category()
    {
        return $this->belongsTo(Vwcategory::class);
    }
    // public function user()
    // {
    //     return $this->belongsTo(Vwuser::class);
    // }
    // public function category()
    // {
    //     return $this->belongsTo('App\Models\Vwcategory', 'category_id', 'id');
    // }
}
