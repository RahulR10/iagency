<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vwvariant extends Model
{
    use HasFactory;

    protected $table = 'vwvariant';

    public function model()
    {
        return $this->belongsTo(Vwmodel::class, 'vwmodel_id');
    }

    public function make()
    {
        return $this->belongsTo(Make::class, 'make_id');
    }

    public function fuel_type()
    {
        return $this->belongsTo(VwfuelType::class, 'vwmfuel_type_id');
    }
}
