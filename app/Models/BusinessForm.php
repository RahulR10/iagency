<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BusinessForm extends Model
{
    use HasFactory, SoftDeletes;

    protected $guarded = ['user_id'];

    protected $appends = [
        'vehicle_category_name',
        'vehicle_subcategory_name',
        'variant_name', 
        'fuel_type',
        'model_name',
        'make_name',
        'rto_name',
        'prev_insurer_name',
        'insurer_name',
        'insurer_logo',
        'expire_in',
        'broker_name',
        'sub_agent_name',
        "policy_pdf_url",
        "cr_amount",
        "cp_amount",
        "pnl_amount",
        "user_name",
        "user_mobile"
    ];

    public function getVehicleCategoryNameAttribute()
    {
        return !empty($this->category->name) ? $this->category->name : '';
    }

    public function getVehicleSubcategoryNameAttribute()
    {
        return !empty($this->subcategory->name) ? $this->subcategory->name : '';
    }

    public function getVariantNameAttribute()
    {
        return !empty($this->variant->name) ? $this->variant->name : '';
    }

    public function getModelNameAttribute()
    {
        return !empty($this->variant->model->name) ? $this->variant->model->name : '';
    }

    public function getFuelTypeAttribute()
    {
        return !empty($this->variant->fuel_type->name) ? $this->variant->fuel_type->name : '';
    }

    public function getMakeNameAttribute()
    {
        return !empty($this->variant->model->make->name) ? $this->variant->model->make->name : '';
    }

    public function getRtoNameAttribute()
    {
        return !empty($this->rto->name) ? $this->rto->rto_num . ' - ' . $this->rto->name : '';
    }
    
    public function getPrevInsurerNameAttribute()
    {
        return !empty($this->prev_insurer->name) ? $this->prev_insurer->name : '';
    }

    public function getInsurerNameAttribute()
    {
        return !empty($this->insurer->name) ? $this->insurer->name : '';
    }

    public function getInsurerLogoAttribute()
    {
        return !empty($this->insurer->image) ? url("images/company/" . $this->insurer->image) : '';
    }

    public function getBrokerNameAttribute()
    {
        return !empty($this->broker->name) ? $this->broker->name : '';
    }

    public function getSubAgentNameAttribute()
    {
        return !empty($this->sub_agent->name) ? $this->sub_agent->name : '';
    }

    public function getPolicyPdfUrlAttribute()
    {
        return !empty($this->policy_pdf) ? url("uploads/business-form/" . $this->policy_pdf) : '';
    }

    public function getUserNameAttribute()
    {
        return !empty($this->user->name) ? $this->user->name : '';
    }

    public function getUserMobileAttribute()
    {
        return !empty($this->user->mobile) ? $this->user->mobile : '';
    }

    public function getExpireInAttribute()
    {
        if(!empty($this->policy_end_date)) {
            $currentDate = time();
            $expiryDate = strtotime($this->policy_end_date);
            
            if($currentDate > $expiryDate) {
                $days = floor(($currentDate - $expiryDate) / (60 * 60 * 24));
                return [
                    'days' => $days,
                    'msg' => $days > 0 ? 'Expired' : 'Expiring In'
                ];
            } else {
                return [
                    'days' => floor(($expiryDate - $currentDate) / (60 * 60 * 24)),
                    'msg' => 'Expiring In'
                ];
            }
            
        } else {
            return [
                'days' => 0,
                'msg' => 'Policy date is not available'
            ];
        }
    }
    
    public function getCrAmountAttribute() {
        $od     = $this->commission_receivable == 'Percent' ? round($this->od_premium * $this->cr_od_premium / 100) : round($this->cr_od_premium);
        $tp     = $this->commission_receivable == 'Percent' ? round($this->tp_premium * $this->cr_tp_premium / 100) : round($this->cr_tp_premium);
        $net    = $this->commission_receivable == 'Percent' ? round($this->net_premium * $this->cr_net_premium / 100) : round($this->cr_net_premium);
        $payout_in = (float) round($od + $tp + $net);
        
        return compact('od', 'tp', 'net', 'payout_in');
    }
    
    public function getCpAmountAttribute() {
        $od     = $this->commission_payable == 'Percent' ? round($this->od_premium * $this->cp_od_premium / 100) : round($this->cp_od_premium);
        $tp     = $this->commission_payable == 'Percent' ? round($this->tp_premium * $this->cp_tp_premium / 100) : round($this->cp_tp_premium);
        $net    = $this->commission_payable == 'Percent' ? round($this->net_premium * $this->cp_net_premium / 100) : round($this->cp_net_premium);
        $payout_out = round($od + $tp + $net);
        return compact('od', 'tp', 'net', 'payout_out');
    }
    
    public function getPnlAmountAttribute() {
        $od     = $this->commission_receivable == 'Percent' ? round($this->od_premium * $this->cr_od_premium / 100) : round($this->cr_od_premium);
        $tp     = $this->commission_receivable == 'Percent' ? round($this->tp_premium * $this->cr_tp_premium / 100) : round($this->cr_tp_premium);
        $net    = $this->commission_receivable == 'Percent' ? round($this->net_premium * $this->cr_net_premium / 100) : round($this->cr_net_premium);
        $payout_in = round($od + $tp + $net);
        
        $od     = $this->commission_payable == 'Percent' ? round($this->od_premium * $this->cp_od_premium / 100) : round($this->cp_od_premium);
        $tp     = $this->commission_payable == 'Percent' ? round($this->tp_premium * $this->cp_tp_premium / 100) : round($this->cp_tp_premium);
        $net    = $this->commission_payable == 'Percent' ? round($this->net_premium * $this->cp_net_premium / 100) : round($this->cp_net_premium);
        $payout_out = round($od + $tp + $net);
        
        return round($payout_in - $payout_out);
    }

    public function category() {
        return $this->belongsTo(Type::class, 'vehicle_category_id');
    }

    public function subcategory() {
        return $this->belongsTo(Type::class, 'vehicle_subcategory_id');
    }

    public function variant() {
        return $this->belongsTo(Vwvariant::class, 'vwvariant_id');
    }

    public function rto()
    {
        return $this->belongsTo(Rto::class);
    }

    public function prev_insurer()
    {
        return $this->belongsTo(Company::class, 'prev_isurer_id');
    }

    public function insurer()
    {
        return $this->belongsTo(Company::class, 'isurer_id');
    }

    public function broker()
    {
        return $this->belongsTo(Agent::class, 'broker_id');
    }

    public function sub_agent()
    {
        return $this->belongsTo(Agent::class, 'sub_agent_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
