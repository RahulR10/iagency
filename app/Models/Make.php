<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Make extends Model
{
    use HasFactory;
    protected $table = "make";

    public function vwmodel()
    {
        return $this->hasMany(vwmodel::class);
    }

    public function variant()
    {
        return $this->hasMany(Vwariant::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }
}
