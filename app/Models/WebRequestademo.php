<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebRequestademo extends Model
{
    use HasFactory;
    public function web_product()
    {
        return $this->belongsTo(WebProduct::class);
    }
}
