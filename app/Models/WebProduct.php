<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebProduct extends Model
{
    use HasFactory;

    public function web_pricing()
    {
        return $this->hasOne(WebPricing::class);
    }
}
