<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VwfuelType extends Model
{
    use HasFactory;

    protected $table = 'vwfuel_type';
}
