<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Media extends Model
{
    use HasFactory;

    protected $appends = ["docs_url"];

    public function getDocsUrlAttribute()
    {
        return $this->docs != "" ? url('storage/' . $this->docs) : "";
    }
}
