<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $with = ['category'];

    public function category()
    {
        return $this->hasOne('App\Models\Type', 'id', 'type_id');
    }

    public function subcategories()
    {
        return $this->hasMany(Type::class, 'type_id', 'id');
    }

    public function quotation()
    {
        return $this->hasMany(Quotation::class);
    }

    public function make()
    {
        return $this->hasMany(Make::class);
    }
}
