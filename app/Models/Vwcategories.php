<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vwcategories extends Model
{
    use HasFactory;

    protected $with = ['vehicle'];

    public function vehicle()
    {
        return $this->hasMany('App\Model\Vwvehicle', 'category_id', 'id');
        // return $this->belongsTo(Vwvehicle::class);
    }
}
