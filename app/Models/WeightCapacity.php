<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeightCapacity extends Model
{
    protected $table = 'weightcapacities';
    protected $guarded = [];
    use HasFactory;

    public function category()
    {
        return $this->hasOne('App\Models\Type', 'id', 'type_id');
    }
}
