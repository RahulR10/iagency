<?php

namespace App\Models;

use Intervention\Image\Facades\Image;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomImage extends Model
{
    use HasFactory;

    public static function getImage($image, $data = array(), $output = null)
    {
        ini_set('max_execution_time', -1);
        extract($data); // array to variables

        $imageArr = explode("?", $image);
        unset($imageArr[ count($imageArr) - 1 ]);
        $image = implode("?", $imageArr);
        // dd($image);

        $img = Image::make($image);  
        $img->text($name , 450, 1200, function($font) {  
            $font->file(public_path('fonts/OpenSans-Bold.ttf'));  
            $font->size(28);  
            $font->color('#ffffff');  
            $font->align('center');  
            $font->valign('bottom');  
            // $font->angle(90);  
        });

        $img->text($mobile, 450, 1230, function($font) {  
            $font->file(public_path('fonts/OpenSans-Regular.ttf'));  
            $font->size(24);  
            $font->color('#ffffff');  
            $font->align('center');  
            $font->valign('bottom');  
            // $font->angle(90);  
        });  

        $img->text($email, 450, 1260, function($font) {  
            $font->file(public_path('fonts/OpenSans-Regular.ttf'));  
            $font->size(24);  
            $font->color('#ffffff');  
            $font->align('center');  
            $font->valign('bottom');  
            // $font->angle(90);  
        });  

        // dd($img->encode('data-url')->encoded);

        // $output = public_path('images/hardik3.jpg');
        // $img->save($output);

        // return $output;

        return $img->encode('data-url')->encoded;
    }
}
