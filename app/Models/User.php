<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */

    protected $table = "users";
    protected $guarded = [];
    protected $with = ['address', 'subscription_plan'];
    protected $appends = ['expires_in', 'status_text', 'full_address','full_referal'];

    // protected $fillable = [
    //     'name',
    //     'email',
    //     'password',
    // ];
    

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute()
    {
        return $this->name . ' ' . $this->mobile;
    }
    
    protected function getExpireIn()
    {
        $expiresIn = 0;
        
        if($this->valid_upto)
        {
            $now = time();
            $expiryDate  = strtotime($this->valid_upto);

            $datediff = $expiryDate - $now;

            $expiresIn = round($datediff / (60 * 60 * 24));
        }
        
        return $expiresIn;
    }

    public function getExpiresInAttribute()
    {
        $expiresIn = $this->getExpireIn();
        return $expiresIn >= 0 ? $expiresIn : 0; // 'Expired';
    }
    
    public function getStatusTextAttribute()
    {
        $status = "Under Trial Period";
        
        $expiresIn = $this->getExpireIn();
        
        if($this->subscription_plan) {
            $status = $this->subscription_plan->name;
        } elseif($expiresIn < 0) {
            $status = "Trial Expired";
        }
        
        return $status;
    }

    public function getFullAddressAttribute()
    {
        return $this->address ? "{$this->address->village}, {$this->address->postoffice},{$this->address->district} ({$this->address->state}), {$this->address->pincode}"  : null;
    }

    public function signature()
    {
        return $this->hasOne(Signature::class);
    }
    
    public function address()
    {
        return $this->belongsTo(Address::class);
    }

    public function subscription_plan()
    {
        return $this->belongsTo(SubscriptionPlan::class);
    }

    public function getFullReferalAttribute()
    {
        return $this->referal_user ? "{$this->referal_user->name}" : null;
    }

    public function referals()
    {
        return $this->hasMany(User::class, 'accept_code', 'referal_code');
    }

    public function quotation()
    {
        return $this->hasMany(Quotation::class);
    }

    public function business_form()
    {
        return $this->hasMany(BusinessForm::class);
    }
}
