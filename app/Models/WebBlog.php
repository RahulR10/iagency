<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebBlog extends Model
{
    use HasFactory;

    public function web_category()
    {
        return $this->belongsTo(WebCategories::class);
    }
}
