<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebPricing extends Model
{
    use HasFactory;

    public function web_product()
    {
        return $this->belongsTo(WebProduct::class);
    }
}
