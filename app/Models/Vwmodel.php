<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vwmodel extends Model
{
    use HasFactory;
    protected $table = "vwmodel";

    public function make()
    {
        return $this->belongsTo(Make::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

}
