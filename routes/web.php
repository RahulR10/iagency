<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/clear-cache', function () {
    return Artisan::call('optimize:clear');
});
// Route::group(['namespace' => 'api'], function () {;
//     Route::get('/q/{token}', 'QuotationController@quotation_pdf2');
// });
Route::get('/migrate', function () {
    return Artisan::call('migrate');
});

Route::get('/seeder', function () {
    $class = request()->get('class');
    return Artisan::call('db:seed --class='.$class);
});

Route::group(['middleware' => 'guest', 'prefix' => 'admin'], function () {
    Route::any('/login', 'LoginController@index')->name('login');
    Route::post('main/checklogin', 'LoginController@checklogin')->name('checklogin');
});

Route::group(['middleware' => 'auth', 'prefix' => 'admin'], function () {
    Route::get('logout', 'LoginController@logout')->name('logout');
    Route::get('/', 'DashboardController@index')->name('admin-home');
    Route::post('/', 'DashboardController@import_vehicle_master')->name('import_vehicle_master');

    Route::group(['as' => 'webadmin.','prefix'=>'web'], function () {
        Route::resources([
            'slider' => 'WebSliderController',
            'page'  => 'WebPageController',
            'price' => 'WebPricingController',
            'product' => 'WebProductController',
            'requestademo' => 'WebRequestADemoController',
            'testimonial' => 'WebTestimonialController',
            'contact' => 'WebContactController',
            'blog' => 'WebBlogController',
            'category' => 'WebCategoriesController'
        ]);
    });
});


Route::group(['middleware' => 'auth', 'as' => 'admin.', 'prefix' => 'admin'], function () {
    Route::get('select_admin', 'DashboardController@select_admin');
    Route::get('/select_admin', function (Request $request) {
        // Retrieve a piece of data from the session...
        $value = session('key');
    
        // Specifying a default value...
        $value = session('key', 'default');
    
        // Store a piece of data in the session...
        session(['key' => $request->value]);
    });

    Route::resources([
        'make' => 'MakeController',
        'vmodel' => 'VModelController',
        'variant' => 'VwvariantController',
        'fuel' => 'FuelTypeController',
        'age' => 'AgeController',
        'cubiccapacity' => 'CubicCapacityController',
        'seatcapacity' => 'SeatCapacityController',
        'weightcapacity' => 'WeightCapacityController',
        'idv' => 'IdvrateController',
        'price' => 'PriceController'
    ]);
    Route::get('idv-list/{category}', 'IdvrateController@master')->name('idv-list');
    Route::get('price-list/{category}', 'PriceController@master')->name('price-list');

    Route::resources([
        'business_form' =>'BusinessFormController',
        'quotation' =>'QuotationController',
        'slider' => 'SliderController',
        'tool' => 'ToolController',
        'type' => 'TypeController',
    ]);

    // Route::group(['prefix' => 'vehicle_wallet',], function () {
        Route::get('vw_vehicle/{id}', 'rtoadmin\VehicleController@detail')->name('vw_vehicledetail');
        Route::get('vw_setting', 'rtoadmin\SettingController@edit')->name('vw_editsetting');
        Route::post('vw_setting', 'rtoadmin\SettingController@update')->name('vw_updatesetting');
        Route::resources([
            'vw_slider'     => 'rtoadmin\SliderController',
            'vw_poster'     => 'rtoadmin\PosterController',
            'vw_category'   => 'rtoadmin\CategoryController',
            'vw_document'   => 'rtoadmin\DocumentController',
            'vw_article'    => 'rtoadmin\ArticleController',
            'vw_renew'      => 'rtoadmin\RenewController',
            'vw_otherapp'   => 'rtoadmin\OtherappController',
            'vw_page'       => 'rtoadmin\PageController',
            'vw_feedback'   => 'rtoadmin\FeedbackController',
            'vw_user'       => 'rtoadmin\UserController',
            'vw_vehicle'    => 'rtoadmin\VehicleController',
        ]);
    // });
    Route::resources([
        'company' => 'CompanyController',
        'healthzone' => 'HealthZoneController',
        'familysize' => 'FamilySizeController',
        'healthage' => 'HealthAgeController',
        'healthplan' => 'HealthPlanController',
        'healthplanprice' => 'HealthPlanPriceController',
    ]);
    Route::get('health/master', 'HealthPlanPriceController@master');

    Route::group(['prefix' => 'user'], function () {
        Route::get('/index', 'UserController@index')->name('users.index');
        Route::get('/', 'UserController@list')->name('users-list');
        Route::put('/{user}', 'UserController@update')->name('users-update');
        Route::get('/export', 'UserController@export')->name('users-export');
    });


    Route::group(['prefix' => 'profile'], function () {
        Route::get('/', 'LoginController@profile')->name('profile');
        Route::post('/', 'LoginController@edit_profile')->name('profile');
    });

    Route::group(['prefix' => 'setting'], function () {
        Route::get('/', 'SettingController@edit')->name('setting');
        Route::post('/', 'SettingController@update')->name('setting');
    });

    Route::post('ajex/changeoption', 'AjexController@changeoption')->name('ajex');
});

Route::group(['namespace' => 'web'],function(){

    #web Routes
    Route::get('/', 'HomeController@index');
    // Route::get('blog','BlogController@blog')->name('blog');
    Route::post('requestademo','RequestademoController@store')->name('request.store');
    Route::resource('blog','BlogController')->only(['index','show'])->parameters([
        'blog'  => 'slug'
    ]);
});